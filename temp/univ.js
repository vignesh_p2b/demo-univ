const express = require('express');
const router = express.Router();

var Issuer = require('../models/issuer')
var Univ = require('../models/univ')
var Student = require('../models/student')
var authsys = require('../cjs/authsys')
var Doc = require('../models/doc')
var Info = require('../models/info')

const multer  = require('multer');
var upload = multer({ dest: 'public/uploads/' })

const fs  = require('fs');
const hashFiles = require('hash-files');
const request = require('request');
const SHA256 = require("crypto-js/sha256");
const jsrsaSign = require('jsrsasign');

const { Environment } = require('storj'); 

/********* STORJ **********/
// Storj Session
/*  UC for server*/
const storj = new Environment({
  bridgeUrl: 'https://api.storj.io',
  bridgeUser: 'vignesh679@gmail.com',
  bridgePass: 'testpasswd',
  encryptionKey: 'hope gallery soft bleak shallow foster federal abandon collect bone town night',
  logLevel: 4
});
// Storj variables
var bucketId = '1b34ef3d5e1a39ea5eef551b';
//

/********* Routes **********/
router.get('/new', authsys.ensureAuthenticated, function(req, res){
  Univ.findOne({dcid: req.user.id}, function(err, result){
    // console.log(result)
    req.session['univ'] = result
    //console.log('req.session.univ: ', req.session['univ'])
    res.render('univ/new',{layout: 'univ.hbs', title: 'Search Student', pHead: 'Search Student', data: result})
  })
})

router.get('/course', authsys.ensureAuthenticated, function(req, res){
  Univ.findOne({dcid: req.user.id}, function(err, result){
    //console.log(result.id + ' profile data: ', result)
    res.render('univ/course',{layout: 'univ.hbs', title: 'Course', pHead: 'Courses and Subjects', data: result})
  })
})

router.post('/course/add', authsys.ensureAuthenticated, function(req, res){
  console.log(req.body)
  Univ.updateCourse(req.user.id, req.body, function(err){
    if(err){
      console.log(err)
      req.flash('error',err)
      res.redirect('/univ/course')
    }
    else{
      //console.log('Updated Univ profile: ', result)
      req.flash('success','Course details updated')
      res.redirect('/univ/course')
    }
  })
})

router.get('/student/', authsys.ensureAuthenticated, function(req, res){
  Univ.findOne({dcid: req.user.id}, function(err, result){
    res.render('univ/student',{layout: 'univ.hbs', title: 'Student', pHead: 'Student', data: result})
  })
})

router.get('/student/:id', function(req, res){
  console.log(req.params.id)
  Student.findById(req.params.id, function(err, result){
    if(err){
      console.log(err)
      req.flash('error', err)
      res.redirect('/univ/student')
    }
    else{
      console.log(result)
      res.render('univ/profile',{layout: 'univ.hbs', title: 'Student Profile', pHead: 'Student Profile', data: result})
    }
  })
})

router.post('/student/add', upload.single('fsub'), authsys.ensureAuthenticated, function(req, res){
  //console.log('Given value', req.body)
  //console.log('File Attributes', req.file)
  var filename = req.body.sfname +'_'+ req.body.slname +'_'+ Date.now();
  
  var data = {};
  data.student = {};
  data.father = {};
  if(req.body.sfname) data.student.fname = req.body.sfname
  if(req.body.smname) data.student.mname = req.body.smname
  if(req.body.slname) data.student.lname = req.body.slname
  if(req.body.ffname) data.father.fname = req.body.ffname
  if(req.body.fmname) data.father.mname = req.body.fmname
  if(req.body.flname) data.father.lname = req.body.flname
  if(req.body.dob) data.dob = req.body.dob
  if(req.body.email) data.email = req.body.email
  if(req.body.phoneno) data.phoneno = req.body.phoneno
  //console.log(data)

  Student.doesStudentExist(data, function(err, exist){
    if(err) console.log(err)
    else if (exist){
      req.flash('error', 'The student already exists!')
      res.redirect('/univ/student')
      fs.unlink('./public/uploads/'+req.file.filename, (err) => {
        if (err) throw err;
        console.log(req.file.filename + ' was deleted');
      });
    }
    else{
      fs.rename('./public/uploads/'+req.file.filename, './public/students/'+ filename, (err) => {
        if (err) throw err;
        console.log('Rename complete!');
      }); 
      
      // updataing data object
      data.pic = filename
      // finalising data object for student creation
      data = new Student(data)
      // creating student object in DB
      Student.createStudent(data, function(err, student){
        if(err) {
          console.log(err)
          req.flash('error', err)
					res.redirect('/univ/student')
        }
        else{
          console.log(student)
          req.flash('success', 'A student account has been successfully created')
					res.redirect('/univ/student/'+student.id)
        }
      })
    }
  })
})

router.post('/student/enroll', authsys.ensureAuthenticated, function(req, res){
  //console.log(req.body)
  Student.joinCourse(req.body, req.user.name, function(err, result){
    if(err && err != 'exist'){
      console.log(err)
      req.flash('error', err)
      res.redirect('/univ/student')
    }
    else if(err == 'exist'){
      req.flash('error', 'The Student is already enrolled for this course')
      res.redirect('/univ/student')
    }
    else{
      req.flash('success', 'The Student is successfully enrolled')
      //res.send(result)
      res.redirect('/univ/student/'+result.id)
    }
  })
})

router.post('/student/list', authsys.ensureAuthenticated, function(req, res){
  // console.log(req.body)
  Student.findStudent(req.body, req.user.name, function(err, result){
    if(err){
      console.log(err)
      req.flash('error', err)
    }
    if(result.length){
      if(req.body.regid){
        // console.log(result)
        //res.send(result)
        res.redirect('/univ/student/' + result[0].id)
      }
      else{
        //res.send(result)
        res.render('univ/list',{layout: 'univ.hbs', title: 'Search Result', pHead: 'Student List', data: result})
      }
    }
    else{
      req.flash('error', 'Found no students matching criteria')
      
      res.redirect('/univ/student')
    }
  })
})

router.post('/updoc', authsys.ensureAuthenticated, function(req, res){
  console.log('body parameters', req.body)
  Student.findStudent(req.body, req.user.name, function(err, result){
    if(err){
      console.log(err)
      req.flash('error', err)
    }
    if(result.length){
      if(req.body.regid){
        // console.log(result)
        //res.send(result)
        var stud = result[0].course[0].regid
        //console.log('session id: ', stud)
        req.session[stud] = result[0]
        //console.log('Session values: req.session[stud]: ', req.session[stud])
        res.render('univ/updoc',{layout: 'univ.hbs', title: 'Upload Document - 2/5', pHead: 'Upload Document To The Selected Student', data: result[0]})
      }
      else{
        //res.send(result)
        res.render('univ/list',{layout: 'univ.hbs', title: 'Search Result', pHead: 'Student List', data: result})
      }
    }
    else{
      req.flash('error', 'Found no students matching criteria')
      res.redirect('/univ/new')
    }
  })
})

router.get('/docdetail/:id', authsys.ensureAuthenticated, function(req, res){
  //console.log('Student id: ', req.params.id)
  //console.log('Session data: ', req.session)
  if(req.session[req.params.id])
    Univ.findOne({dcid: req.user.id}, function(err, result){
      if(err) console.log(err)
      else
        res.render('univ/docdetail',{layout: 'univ.hbs', title: 'Search Result', pHead: 'Student List', data: req.session[req.params.id], univ: result})
    })
  else
    res.redirect('/univ/new')
})

router.post('/pandp/:id', upload.single('fsub'), authsys.ensureAuthenticated, function(req, res, next){
  // console.log('File Attributes', req.file)
  // console.log('Body Attributes', req.body)
  if(!req.session[req.params.id]){
    console.log('Session doesnot exist')
    res.redirect('/univ/new')
  }
  else{
  // Document Session data
  var doc = {};
  doc.receiverId = req.session[req.params.id]._id
  doc.issuer     = req.user.name
  doc.issuerId   = req.user.id
  doc.student    = req.session[req.params.id].student.fname + ' ' + req.session[req.params.id].student.mname + ' ' + req.session[req.params.id].student.lname;
  //doc.dob        = req.session[req.params.id].dob;
  //doc.pic        = req.session[req.params.id].pic;
  // Student course info
  doc.regid      = req.session[req.params.id].course[0].regid;
  doc.degType    = req.session[req.params.id].course[0].degType;
  doc.degree     = req.session[req.params.id].course[0].degree;
  doc.course     = req.session[req.params.id].course[0].course;
  doc.year       = req.session[req.params.id].course[0].yearEnd;
  // Document info
  doc.udocId     = req.body.certId;
  doc.doi        = req.body.idate;
  doc.docType    = req.body.certType;
  doc.filename   = req.session[req.params.id]._id + '_' +Date.now()
  // Univ info
  doc.signatory  = req.body.signatory

  hashFiles({files: './public/uploads/'+req.file.filename, algorithm: 'sha256'}, function(error, hash){
    if (error)  console.log(error)
    else {
      //console.log(hash)
      doc.hash = hash;
      
      Doc.doesDocExist(hash, function(err, docResult){
        if(err) console.log(err)
        else{
          if(docResult){
            req.flash('error', 'The document already exist!')
            fs.unlink('./public/uploads/'+req.file.filename, (err) => {
              if (err) throw err;
              console.log(req.file.filename + ' was deleted');
            });
            res.redirect('/univ/docdetail/' + req.params.id)
          }
          else{
            console.log(doc)
            req.session.doc = doc
            var oldpath = './public/uploads/'+req.file.filename
            var newpath = './public/'+ req.user.id
            res.render('univ/pandp', {layout: 'univ.hbs', title: 'Preview', pHead: 'Preview and Sign', data: req.session[req.params.id], doc: doc, user: req.user});  
            fs.stat('./public/'+ req.user.id, function(err, stat){
              if(err){
                if(err.code == 'ENOENT')
                  fs.mkdir(newpath, 0o777, function(err){
                    if(err) console.log(err)
                    else
                      fs.rename(oldpath, newpath +'/'+ doc.filename, (err) => {
                        if (err) throw err;
                        console.log('file move complete!');
                      });
                  })
              }
              else
                fs.rename(oldpath, newpath +'/'+ doc.filename, (err) => {
                  if (err) throw err;
                  console.log('file move complete!');
                });
            })
          }
        }
      })
    }
  })
  }
})

/*
router.post('/confirm/:id', authsys.ensureAuthenticated, function(req, res){
  console.log(req.body)
  var docdata = req.session.doc
  //var studData = req.session[req.params.id]
  var filePath = './public/'+ req.session.doc.issuerId + '/' + req.session.doc.filename;
  ************ UC for Server ***********
  Issuer.removeOneSlot(req.user.email, function(uerr, hasSlot){
    if(uerr && uerr != 'noSlot'){
      console.log(uerr)
    }
    if(uerr == 'noSlot'){
      req.flash('error', "You have 0 slot. Please purchase more to upload documents.")
    }
    if(hasSlot){
      hashFiles({files: filePath, algorithm: 'sha256'}, function(error, hash){
        if (error)  console.log(error)
        else {
          // console.log('Hash : ',hash)
          const state = storj.storeFile(bucketId, filePath, {
            filename: req.session.doc.filename,
            progressCallback: function(progress, downloadedBytes, totalBytes) {
            //console.log('progress:', progress);
            },
            finishedCallback: function(err, fileId) {
              if (err) return console.error(err);
              // console.log('File complete:', fileId);
              request({
                url: 'http://172.104.170.157:3000/api/certificate',
                method: 'POST',
                json: {
                  "$class": "org.doc2chain.certificate",
                  "certId": req.session.doc.udocId,
                  "encHash": req.body.signature,
                  "fileId": fileId
                }
              }, function(reqErr, reqRes, reqBody){
                // console.log('response', reqRes);
                // console.log('body', reqBody);
                var data = {};
                docdata.hashSign = SHA256(req.body.signature);
                docdata.txId = SHA256('TXID'+req.body.signature);
                docdata.storjfileid = fileId
                docdata.p2bId = docdata.hashSign
                res.render('univ/confirm', {layout: 'univ.hbs', data: docdata})
                Doc.findOneAndUpdate({udocId: docdata.docId}, docdata, { "new": true, "upsert": true }, function(err, doc){
                console.log(doc)
                })
              });
            }
          });
        }
      })
    }
  })
})
*/
router.get('/certificate/:id', function(req, res){
  console.log(req.params.id);
  Doc.findOne({p2bId: req.params.id}, function(err, result){
    res.render('univ/certificate', {data: result, title: 'Certificate', verify: false})
  })
})

router.post('/certificate/:id', function (req, res) {
  Doc.findOne({p2bId: req.params.id}, function(err, result){
    console.log('result.udocId: ', result.udocId);
    stat = {};
    //console.log('req.session[result.udocId]: ', req.session[result.udocId]);
		Student.findById(result.receiverId, function(err, info){
			res.render('univ/certificate', {data: result, title: 'Certificate', info: info, verify: true})
		})
    request('http://172.104.170.157:3000/api/certificate/'+result.udocId, function (reqErr, reqRes, reqBody){
			if (reqErr) {
				console.log(reqErr);
				stat[1] = false;
				stat[2] = false;
				stat[3] = false;
				stat[4] = false;
				Info.findOneAndUpdate({p2bId: result.p2bId}, stat, {new: true, upsert: true}, function(serr, sdata){
					//console.log(sdata)
				})
			}
			else{
				console.log('Data from blockchain: ', reqBody);
				//stat.p2bId = result.p2bId
				stat[1] = true;
				console.log('stat: ', stat)
				Info.findOneAndUpdate({p2bId: result.p2bId}, stat, {new: true, upsert: true}, function(serr, sdata){
					if(serr) console.log('serr: ', serr)
					console.log('sdata: ', sdata)
				})
				//console.log('req.session[result.udocId]: ', req.session[result.udocId]);
				reqBody = JSON.parse(reqBody);
				encHash=reqBody.encHash;
				//console.log('Encrypted hash: ', encHash);
				//certId=reqBody.certId;
				//console.log('Certificate ID: ', certId);
				var filePath = './temp/'+result.filename;
				console.log('storjId : ', result.storjfileid)
				// Fetching document from storage
				storj.resolveFile(bucketId, result.storjfileid, filePath, {
				progressCallback: function(progress, downloadedBytes, totalBytes) {},
				finishedCallback: function(stoErr) {
					if (stoErr) {
						console.log(stoErr);
						stat[2] = false;
						stat[3] = false;
						stat[4] = false;
						Info.findOneAndUpdate({p2bId: result.p2bId}, stat, {new: true, upsert: true}, function(serr, sdata){
							//console.log(sdata)
						})
					}
					else{
						console.log('File download complete');
						stat[2] = true;

						// Generating digest of the document
						hashFiles({files: filePath, algorithm: 'sha256'}, function(hashErr, hash) {
							// Clearing tempfile
							fs.stat(filePath, function (err, stats) {
								//console.log(stats);//here we got all information of file in stats variable
								if (err) console.error(err);
								else
									fs.unlink(filePath, function(err){
										if(err) console.log(err);
										console.log('file deleted successfully');
									});
							});
							
							if(hashErr) {
								console.log('file hash: error: ', hashErr);
								stat[3] = false;
								stat[4] = false;
								Info.findOneAndUpdate({p2bId: result.p2bId}, stat, {new: true, upsert: true}, function(serr, sdata){
									console.log(sdata)
								})
							}
							else{
								console.log('Hash succeded', hash);
								stat[3] = true;
								//console.log('req.session[result.udocId]: ', req.session[result.udocId]);
								
								// Verifying signatures
								console.log('IssuerId: ', result.issuerId);
								Issuer.findById(result.issuerId, function(uDBerr, uDBres){
									if (uDBerr) console.log(uDBerr)
									else{
										//console.log('Issuer DB Data: ', uDBres);
										verifySign(uDBres.pubkey, hash, encHash, uDBres.isRSA, function(isValid){
											console.log("Verification: ", isValid)
											stat[4] = isValid;
											console.log('stat: ', stat)
											Info.findOneAndUpdate({p2bId: result.p2bId}, stat, {new: true, upsert: true}, function(serr, sdata){
												
												console.log('sdata: ', sdata)
											})
											//console.log('req.session[result.udocId]: ', req.session[result.udocId]);
										})
									}
								})
							}
						});
					}
				}});
			}
    });
  });
});

router.get('/api/data/:id', function (req, res) {
  Info.findOne({p2bId: req.params.id}, function(err, data){
    if(data){
			//console.log('data', data)
      for(i = 1; i < 6; i++){
        //console.log('data['+i+']', data[i])
        if(data[i] == undefined)  data[i] = 'wait';
      }
			res.json(data)/*
			if(data[4] != undefined){
				console.log('Removing entry')
				Info.findOneAndRemove({p2bId: req.params.id}, function(derr, ddata){})
			}*/
		}
  })
})

router.get('/settings', authsys.ensureAuthenticated, function(req, res){
  Univ.findOne({dcid: req.user.id}, function(err, result){
    res.render('univ/set', {layout: 'univ.hbs', title: 'Settings', data: result, user: req.user}); 
  })
})

router.post('/settings', authsys.ensureAuthenticated, function(req, res){
  console.log('Body parameters: ', req.body)
  var update = {}
  if(req.body.docType)  update.docType = req.body.docType
  if(req.body.signatory)  update.signatory = req.body.signatory
  Univ.findOneAndUpdate({dcid: req.user.id}, {$addToSet: update}, { "new": true, "upsert": true }, function(err, result){
    if(err) console.log(err)
    else
      res.render('univ/set', {layout: 'univ.hbs', title: 'Settings', data: result, user: req.user}); 
  })
})

module.exports = router;



/********** CUSTOM FUNCTIONS ***********/
var verifySign = function(pubkey, msg, signature, isRSA, callback){
  var isValid;
  if(isRSA){
    var sig = new jsrsaSign.crypto.Signature({"alg": "SHA256withRSA"});
    // initialize for signature validation
    sig.init(pubkey); // signer's certificate or pubkey
    // update data or msg
    sig.updateString(msg)
    // verify signature
    isValid = sig.verify(signature)
  }
  else{
    var sig = new jsrsaSign.crypto.Signature({"alg": 'SHA256withECDSA', "prov": "cryptojs/jsrsa"});
    sig.init({xy: pubkey, curve: 'secp256r1'});
    sig.updateString(msg);
    isValid = sig.verify(signature);
  }
  callback(isValid);
}

/******* Test routes and data *******/

router.get('/confirm/', authsys.ensureAuthenticated, function(req, res){
  var docdata ={};
  docdata.udocId = 'testcert003'
  docdata.hashSign = '1234567899gr8r4g89g494g68g1g46g4seg4s'
  docdata.to = 'priyavenkat16295@gmail.com'
  docdata.from = req.user.email
  docdata.issuer = req.user.name
  //docdata.subject = 'Your "'+ docdata.udocId +'" from ' + docdata.issuer
  res.render('univ/confirm', {layout: 'univ.hbs', data: docdata})
})