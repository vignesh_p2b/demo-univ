//**** University profile ****/
var univSchema = new Schema({
  name: { type: String, required: true },
  p2bid: { type: String, required: false },
  type: { type: Array, default: [] },
  deg: { type: Array, default: [] },
  course: { type: Array, default: [] },
  email: { type: String, default: "mail.to.univ@mail.com" },
  docType: { type: Array, default: ["Degree Certificate", "MarkSheet"] },
  Signatory : { type: Array, default: ["VC", "Registrar"] },
  issuerURL : { type: String, default: "https://www.block.institute.com/certificate/" }
});
var univModel = mongoose.model('univ', univSchema);

/******** Student Schema *******/
var studentSchema = new Schema({
  regid: { type: String, required: true },
  student: {
    fname: { type: String, required: true },
    mname: { type: String },
    lname: { type: String }
  },
  father: {
    fname: { type: String },
    mname: { type: String },
    lname: { type: String }
  },
  pic: { type: String },
  dob: { type: String },
  p2bid: { type: String, required: false },
  type: { type: String},
  deg: { type: String },
  course: { type: String },
  univ: { type: String },
  year: { type: String },
  cardno: { type: String },
  phoneno: {type: String },
  email: { type: String, default: "mail.to.student@mail.com" },
  certificates: { type: Array, default: [] }
});
var studModel = mongoose.model('student', studentSchema);

/******** Document Schema ******/
var docSchema = new Schema({
  p2bId : { type: String },
  udocId : { type: String },
  type : { type: String },
  course : { type: String },
  deg : { type: String },
  doi : { type: String },
  issuer : { type: String },
  receiver : { type: String },
  receiverId : { type: String },
  signatory : { type: String },
  hash : { type: String },
  filename : { type: String },
  storjfileid : { type: String },
  filepath : { type: String },
  txId : { type: String },
});
var docModel = mongoose.model('document', docSchema);

