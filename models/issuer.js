/********* Env variables **********/
const bcrypt = require('bcrypt');
const mongoose = require('mongoose');
const Schema = mongoose.Schema;

var url = 'mongodb://localhost:27017/docChain';
mongoose.connect(url);

var db = mongoose.connection;

var Univ = require('./univ');
/********* Schema **********/
var issuerSchema = Schema({
    name: { type: String, required: true },
    email: { type: String, default: "mail.to.univ@mail.com" },
    accType: { type: String, default: 'user' },
    model: { type: String, default: 'user.hbs' },
    edu: { type: Boolean, default: false},
    org: { type: Boolean, default: false},
    eve: { type: Boolean, default: false},
    passwd: { type: String },
    isRSA: { type: Boolean, default: 'false' },
    pubkey: { type: String },
    docs: { type: Number, default: 0 },
    created: { type: Date, default: Date.now }
});
var issuerModel = module.exports = mongoose.model('issuer', issuerSchema);

module.exports.createIssuer = function(newIssuer, callback){
    bcrypt.hash(newIssuer.passwd, 10, function(err, hash) {
        newIssuer.passwd = hash;
        newIssuer.save(callback);
    });
}

module.exports.changepwd = function(email, data, callback){
    //console.log('id : ', id);
    issuerModel.getUserByEmail(email, function(err, user){
        if(err) console.log('Getuser error: ', err);
        if(!user){
            //console.log('Unknown User')
            callback('noUser', null)
        }
        bcrypt.hash(data.passwd, 10, function(err, hash) {
            var update = {passwd: hash}
            issuerModel.findByIdAndUpdate(user.id, update, function(uperr, result){
                if(uperr) console.log('Update Passwd error: ', uperr);
                else{
                    //console.log('Result: ', result)
                    callback(null, 'ok')
                }
            });
        })
    })
}

/*
module.exports.updateIssuer = function(update, callback){
    if(update.passwd)
    bcrypt.hash(update.passwd, 10, function(err, hash) {
        update.passwd = hash;
        issuerModel.findByIdAndUpdate(id, update, callback);
    })
}
*/

module.exports.updatePubkey = function(data, callback){
    issuerModel.getUserByEmail(data.email, function(err, user){
        if(err) console.log(err);
        if(!user){
            //console.log('Unknown User')
            var error = 'noUser'
            callback(error, null)
        }
        issuerModel.comparePassword(data.passwd, user.passwd, function(err, isMatch){
            if(err) console.log(err);
            if(isMatch){
                var update = {};
                update.pubkey = data.pubkey;
                if (data.pubkey.length == 178) update.isRSA = true
                if (data.pubkey.length == 130) update.isRSA = false
                issuerModel.findByIdAndUpdate(user.id, update, callback)
            }
            else{
                //console.log('Invalid Password')
                var error = 'inPass'
                callback(error, null)
            }
        })
    });
};

// Enable or disable user models
module.exports.upgradeAccount = function(data, callback){
    var update = {
        edu: data.edu,
        org: data.org,
        eve: data.eve
    }
    issuerModel.findOneAndUpdate({email: data.email}, update, {new: true}, function(err, issuer){
        callback(err, issuer)
        //console.log('EDU: ', issuer.edu, '\nORG: ', issuer.org, '\nEVE: ', issuer.eve)
        issuerModel.createAppData(issuer)
    })
}

// Add document slots
module.exports.addDocSlots = function(data){
    issuerModel.findOne({email: data.email}, function(err, result){
        var update = {};
        update.docs = Number(result.docs) + Number(data.docs);

        issuerModel.findOneAndUpdate({email: data.email}, update, {new: true}, function(upErr, upResult){
            if(upErr)   console.log(upErr)
            else    console.log(upResult.email+': docs : '+upResult.docs)
        })
    });
}

// Remove one slot
module.exports.removeOneSlot = function(email, callback){
    issuerModel.findOne({email: email}, function(err, result){
        
        if(Number(result.docs) > 0){
            var update = {};
            
            // Decrementing doc slots by 1
            update.docs = Number(result.docs) - 1;

            // Updating the user profile in db
            issuerModel.findOneAndUpdate({email: email}, update, function(upErr, upResult){
                if(upErr)   console.log(upErr)
                else{
                    console.log(upResult.docs)
                }
            })

            // Return all OK
            callback(null, true)
        }
        else{
            // Return no. of slots is 0
            callback('noSlot', null)
        }
    });
}

// Gives a list of users and their data
module.exports.getUserList = function(callback){
    issuerModel.find({accType: 'user'}, callback);
}

module.exports.getUserbyId = function(id, callback){
    //console.log('id : ', id);
    issuerModel.findById(id, callback);
}

module.exports.getUserByEmail = function(email, callback){
    //console.log("email : ", email);
    var query = {email: email};
    //console.log(query);
    issuerModel.findOne(query, callback);
}

module.exports.comparePassword = function(password, hash, callback){
    //console.log("Password: ", password)
    //console.log("Hash: ", hash)
    bcrypt.compare(password, hash, function(err, isMatch) {
        callback(null, isMatch);
    });
}

module.exports.setDefaultModel = function(email, model, callback){
    issuerModel.findOneAndUpdate({email: email}, {model: model}, function(err, result){
        if(err)   {
            console.log(err)
            callback(err)
        }
        else{
            console.log(result.model + ' is as Default model')
            callback(null)
        }
    })
}

module.exports.createAppData = function(issuer){
    // preparing data for object creation
    var data = {};
    data.dcid = issuer.id;

    //console.log('EDU: ', issuer.edu, '\nORG: ', issuer.org, '\nEVE: ', issuer.eve)

    if(issuer.edu){
        // check whether the object already exits
        Univ.doesUnivExist(issuer.id, function(err, exist){
            if(err) console.log(err)
            else if(!exist){
                console.log('Creating Univ object')

                // finalising data for object creation
                data = new Univ(data);

                // Creating University object in DB
                Univ.createUniv(data, function(cerr, univ){
                    if (cerr) console.log(cerr);
                    else{
                    console.log('Univ Object created successfully')
                    console.log(univ);
                    }
                })
            }
            else    console.log('UNIV Object exists')
        })
    }

    if(issuer.org){
        console.log('Organisation model is not yet implemented')
    }

    if(issuer.eve){
        console.log('Event model is not yet implemented')
    }
}