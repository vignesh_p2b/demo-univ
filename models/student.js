/********* Env variables **********/
const mongoose = require('mongoose');
const Schema = mongoose.Schema;

var url = 'mongodb://localhost:27017/docChain';
mongoose.connect(url);

var db = mongoose.connection;

/******** Student Schema *******/
var studentSchema = new Schema({
  student: {
    fname: { type: String },
    mname: { type: String },
    lname: { type: String }
  },
  father: {
    fname: { type: String },
    mname: { type: String },
    lname: { type: String }
  },
  pic: { type: String },
  dob: { type: String },
  course: [{
    regid: { type: String },
    univ: { type: String },
    degType: { type: String},
    degree: { type: String },
    course: { type: String },
    year: { type: String }
    }],
  phoneno: {type: String, required: true },
  email: { type: String, required: true, default: "mail.to.student@mail.com" },
});
var studModel = module.exports = mongoose.model('student', studentSchema);

module.exports.createStudent = function(newStudent, callback){
  newStudent.save(callback);
}

module.exports.doesStudentExist = function(query, callback){
  studModel.findOne(query, function(err, result){
    if(err){
      console.log(err)
      callback(err, null)
    }
    else if (result != null){
      //console.log('student object exist')
      callback(null, result.id)
    }
    else{
      //console.log('student object does not exist')
      callback(null, false)
    }
  });
}

module.exports.findStudent = function(data, univ, callback){
  var query = {}
    if(data.regid)
      query.regid = data.regid
    else{
      if(data.course && data.course != 'None') query.course = data.course
      if(data.degType && data.degType != 'None') query.degType = data.degType
      if(data.degree && data.degree != 'None') query.degree = data.degree
      if(data.year) query.year = data.year
    }
    query.univ = univ
  
  var query1 ={}
    if(data.fname) query1['student.fname'] = data.fname
    if(data.mname) query1['student.mname'] = data.mname
    if(data.lname) query1['student.lname'] = data.lname
  
  //console.log('query', query)
  //console.log('query1', query1)

  var pro = {}
    pro = {'course.$': 1}
    pro.student = 1
    pro.father = 1
    pro.pic = 1
    pro.dob = 1
    pro.email = 1
  //console.log(pro)
  
  if(data.fname || data.mname || data.lname){
    console.log('Query using name')
    studModel.find(query1, function(err, result){ // {course: {$elemMatch: query}}, pro, 
      if(err) {
        console.log(err)
        callback(err, null)
      }
      else{
        console.log(result)
        callback(null, result)
      }
    })
  }
  else{
    console.log('Query using course details')
    studModel.find({course: {$elemMatch: query}}, pro, function(err, result){ // {course: {$elemMatch: query}}, pro, 
      if(err) {
        console.log(err)
        callback(err, null)
      }
      else{
        console.log(result.course)
        callback(null, result)
      }
    })
  }
}

module.exports.joinCourse = function(data, univ, callback){
  var update = {}
    update.regid = data.regid
    update.univ = univ
    update.degType = data.degType
    update.degree = data.degree
    update.course = data.course
    update.year = data.year
  console.log(update)

  studModel.findOne({course: {$elemMatch: update}}, function(err, result){
    if(err) {
      console.log(err)
      callback(err, null)
    }
    else{
      if(result){
        // console.log(result)
        // console.log('User already enrolled')
        callback('exist', null)
      }
      else{
        console.log('User is not yet enrolled')
        studModel.findByIdAndUpdate(data.id, {$push: {course: update}}, { "new": true, "upsert": true }, function(inerr, inresult){
          if(inerr) {
            console.log(inerr)
            callback(inerr, null)
          }
          else{
            console.log(inresult)
            //console.log(result.course)
            callback(null, inresult)
          }
        })
      }
    }
  })
}