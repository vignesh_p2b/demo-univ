/********* Env variables **********/
const mongoose = require('mongoose');
const Schema = mongoose.Schema;

var url = 'mongodb://localhost:27017/docChain';
mongoose.connect(url);

var db = mongoose.connection;
var Student = require('./student');
//**** Educational institute Schema ****/
var univSchema = new Schema({
  dcid: { type: String, required: true, trim: true },
  type: { type: String, default: 'univ'},
  degType: { type: Array, default: [] },
  degree: { type: Array, default: [] },
  course: { type: Array, default: [] },
  courseDetail:[
    {
      degType: { type: String },
      degree: { type: String },
      course: { type: String },
    }
  ],
  docType: { type: Array, default: [] },
  signatory : { type: Array, default: [] },
  signDetail: [
    {
      docType: { type: String },
      signatory : { type: Array, default: [] }
    }
  ],
  issuerURL : { type: String, trim: true, default: "https://www.block.institute.com/certificate/" },
  created: { type: Date, default: Date.now }
});
var univModel = module.exports = mongoose.model('univ', univSchema);

module.exports.createUniv = function(newUniv, callback){
  newUniv.save(callback);
}

module.exports.doesUnivExist = function(id, callback){
  univModel.findOne({dcid: id}, function(err, result){
    if(err){
      console.log(err)
      callback(err, null)
    }
    else if (result != null){
      //console.log('Univ object exist')
      callback(null, true)
    }
    else{
      //console.log('Univ object does not exist')
      callback(null, false)
    }
  });
}

module.exports.updateCourse = function(id, data, callback){
  var update = {};/*
  if(data.degType) update.degType = data.degType
  if(data.degree) update.degree = data.degree
  if(data.course) update.course = data.course*/
  update.degType = data.degType
  update.degree = data.degree
  update.course = data.course
  update.courseDetail = {
    degType: data.degType,
    degree: data.degree,
    course: data.course
  }
  //console.log(update)
  univModel.findOneAndUpdate({dcid: id}, {$addToSet: update}, { "new": true, "upsert": true }, function(err, result){
    if(err){
      console.log(err)
      callback(err)
    }
    else{
      //console.log(result)
      callback(null)
    }
  })
}