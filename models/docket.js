const mongoose = require('mongoose');
const Schema = mongoose.Schema;

var url = 'mongodb://localhost:27017/docChain';
mongoose.connect(url);

var db = mongoose.connection;

/******** Document Schema ******/
var docketSchema = new Schema({
  docketId : { type: String }
})