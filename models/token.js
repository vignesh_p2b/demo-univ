const mongoose = require('mongoose');
const Schema = mongoose.Schema;

var url = 'mongodb://localhost:27017/docChain';
mongoose.connect(url);

var db = mongoose.connection;
var issuer = require('../models/issuer') 
/******** Document Schema ******/
var tokenSchema = new Schema({
  token : { type: String, required: true },
  email: { type: String }
})
var tokenModel = module.exports = mongoose.model('token', tokenSchema);

module.exports.issueToken = function(email, callback){
  issuer.findOne({email: email}, function(cerr, cresult){
    if(cerr) console.log('Getuser error: ', cerr);
    //console.log("Getting issuer data: ", cresult)
    if(!cresult){
      //console.log('Unknown User')
      callback('noUser', null)
    }
    else{
      new tokenModel({
        email : email,
        token : Math.floor(Math.random() * 1000000)
      }).save(function(err, result){
        if(err) {
          console.log("Error creating token: ", err)
          callback(true, null)
        }
        else {
          console.log("Token Details: ", result)
          callback(null, result.token)
        }
      });
    }
  })
}

module.exports.revokeToken = function(id, callback){

}