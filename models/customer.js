/********* Env variables **********/
const mongoose = require('mongoose');
const Schema = mongoose.Schema;

var url = 'mongodb://localhost:27017/docChain';
mongoose.connect(url);

var db = mongoose.connection;

/******** Student Schema *******/
var customerSchema = new Schema({
  student: {
    fname: { type: String },
    mname: { type: String },
    lname: { type: String }
  },
  father: {
    fname: { type: String },
    mname: { type: String },
    lname: { type: String }
  },
  pic: { type: String },
  dob: { type: String },
  phoneno: {type: String, required: true },
  email: { type: String, required: true, default: "mail.to.student@mail.com" },
});
var custModel = module.exports = mongoose.model('customer', customerSchema);

module.exports.createStudent = function(newStudent, callback){
  newStudent.save(callback);
}

module.exports.doesStudentExist = function(query, callback){
  custModel.findOne(query, function(err, result){
    if(err){
      console.log(err)
      callback(err, null)
    }
    else if (result != null){
      //console.log('student object exist')
      callback(null, result.id)
    }
    else{
      //console.log('student object does not exist')
      callback(null, false)
    }
  });
}

module.exports.findStudent = function(data, univ, callback){
  var query = {}
    if(data.id)
      query['_id'] = data.id
    else{
			if(data.fname) query['student.fname'] = data.fname
			if(data.mname) query['student.mname'] = data.mname
			if(data.lname) query['student.lname'] = data.lname
		}
  //console.log('query', query)
  //console.log('query1', query1)
    console.log('Query using name')
    custModel.find(query, function(err, result){ // {course: {$elemMatch: query}}, pro, 
      if(err) {
        console.log(err)
        callback(err, null)
      }
      else{
        console.log(result)
        callback(null, result)
      }
    })
}
/*
module.exports.joinCourse = function(data, univ, callback){
  var update = {}
    update.regid = data.regid
    update.univ = univ
    update.degType = data.degType
    update.degree = data.degree
    update.course = data.course
    update.year = data.year
  console.log(update)

  custModel.findOne({course: {$elemMatch: update}}, function(err, result){
    if(err) {
      console.log(err)
      callback(err, null)
    }
    else{
      if(result){
        // console.log(result)
        // console.log('User already enrolled')
        callback('exist', null)
      }
      else{
        console.log('User is not yet enrolled')
        custModel.findByIdAndUpdate(data.id, {$push: {course: update}}, { "new": true, "upsert": true }, function(inerr, inresult){
          if(inerr) {
            console.log(inerr)
            callback(inerr, null)
          }
          else{
            console.log(inresult)
            //console.log(result.course)
            callback(null, inresult)
          }
        })
      }
    }
  })
}
*/
