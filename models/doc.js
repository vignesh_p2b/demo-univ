/********* Env variables **********/
const mongoose = require('mongoose');
const Schema = mongoose.Schema;

var url = 'mongodb://localhost:27017/docChain';
mongoose.connect(url);

var db = mongoose.connection;

/******** Document Schema ******/
var docSchema = new Schema({
  p2bId : { type: String },
  udocId : { type: String },
  issuer : { type: String },
  issuerId : {type: String },
  docType : { type: String },

  regid : { type: String },
  course : { type: String },
  degree : { type: String },
  year: { type: String },
  doi : { type: String },
  
  //receiver : { type: String },
  receiverId : { type: String },
  signatory : { type: Array, default: [] },

  hash : { type: String },
  hashSign : { type: String },

  filename : { type: String },
  
  storjfileid : { type: String },
  txId : { type: String },

  PAtxId: { type: String },
  PAleafId : { type: String} 
});
var docModel = module.exports = mongoose.model('document', docSchema);

module.exports.createDoc = function(newDoc, callback){
  newDoc.save(callback);
}

module.exports.doesDocExist = function(hash, callback){
  docModel.findOne({hash: hash}, {hash: 1}, callback)
}
