const passport = require('passport')
const LocalStrategy = require('passport-local').Strategy
const SHA256 = require("crypto-js/sha256");

const User = require('../models/issuer')

passport.serializeUser(function(user, done) {
  console.log('serialising user ', user.id)
  done(null, user.id);
});

passport.deserializeUser(function(id, done) {
  console.log('deserialising user ', id)
  User.getUserbyId(id, function(err, user) {
    done(err, user);
  })
})

module.exports = passport.use(new LocalStrategy({ usernameField: 'email', passwordField: 'passwd'}, function(username, password, done){
  console.log('Email: ', username, ' Password', password)
  User.getUserByEmail(username, function(err, user){
    //console.log('user: ', user)
    if(err){
      console.log(err)
      throw err;
    } 
    if(!user) return done(null, false, {message: 'Unknown User'})
    console.log('user: ', user.passwd)/*
    if (user.passwd != SHA256(password))
      return done(null, false, {message: 'Invalid Password'})
    else{
      User.createAppData(user)
      return done(null, user);
    }
    */

    User.comparePassword(password, user.passwd, function(err, isMatch){
      if(err) throw err;
      if(isMatch)
        return done(null, user);
      else
        return done(null, false, {message: 'Invalid Password'})
    })
  });
}));

module.exports.ensureAuthenticated = function(req, res, next){
  //console.log(req)
  if(req.isAuthenticated())   return next()
  res.redirect('/login') 
}

module.exports.isLoggedIn = function(req, res, next){
  //if(req.isAuthenticated()) res.redirect('back')  //console.log(req.header('Referer'))
  //console.log('Is logged in', req.isAuthenticated())
  if(req.isAuthenticated()){
    //console.log('Account type', req.user.accType)
    if(req.user.accType=='admin') return res.redirect('/admin')
    //if(req.user.accType=='user') 
    res.redirect('/org')
  }
  else return next()
}

module.exports.isAdmin = function(req, res, next){
  //console.log(req.user)
  if(req.user.accType == 'admin') return next()
  res.redirect('/')
}