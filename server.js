const express = require('express');
const morgan = require('morgan');
const bodyParser = require('body-parser');
const validator = require('express-validator');
const hbs = require('hbs');
const expressHbs = require('express-handlebars');
const mongoClient = require('mongodb').MongoClient;
const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const multer  = require('multer');
const bcrypt = require('bcrypt');
const cookieParser = require('cookie-parser');
const passport = require('passport');
const LocalStrategy = require('passport-local').Strategy;
const session = require('express-session');
const flash = require('connect-flash');
const redis   = require("redis");
const redisStore = require('connect-redis')(session);
const client  = redis.createClient();
var expressValidator = require('express-validator');

/********** Application Configuration ********/
const app = express();
var port = 9000;

var index = require('./routes/index');
var admin = require('./routes/admin');
var user = require('./routes/user');
var univ = require('./routes/univ');

/********* MIDDLEWARE CONFIG **********/ 
app.engine('.hbs', expressHbs({defaultLayout: 'homeLayout', extname: '.hbs'}));
app.set('view engine', 'hbs');
app.use(express.static(__dirname + '/public'));
app.use(cookieParser());
app.use(morgan('dev'));
app.use(expressValidator())

var upload = multer({ dest: 'public/uploads/' })

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: true}));

// Handle Sessions
app.use(session({
  secret: 'docchainsessionsecret',
  // create new redis store.
  store: new redisStore({ host: 'localhost', port: 6379, client: client, ttl :  260}),
  saveUninitialized: false,
  rolling: true,
  resave: false
}));

// Passport
app.use(passport.initialize());
app.use(passport.session());

// Connect flash
app.use(flash());

// Session-persisted message middleware
app.use(function(req, res, next){
  res.locals.message = req.flash();
  console.log('Respond Local: ', res.locals)
  next();
});

app.use(function(req, res, next) {
  if (req.user) {
      res.header('Cache-Control', 'private, no-cache, no-store, must-revalidate');
      res.header('Expires', '-1');
      res.header('Pragma', 'no-cache');
  }
  next();
});

/********* ROUTES **********/
app.use('/', index);
app.use('/univ', univ)
app.use('/admin', admin)
app.use('/user', user)

app.use(function(req, res) {
  res.render('main/404', {title: 'DocChain - 404'})
});

app.listen(port, function () {
  console.log('App listening on port '+port+'!')
});
