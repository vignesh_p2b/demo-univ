/********** API ROUTES ********/

// Fetching all document data from database
router.get('/api/fetch.db.doc/', function (req, res) {
  console.log("Fetching all document data from DB")
	Doc.find({}, function(err, result){
		if(err)	console.log(err)
		else	res.json(result)
	})
})

// Fetching data from database
router.get('/api/fetch.db.doc/:id', function (req, res) {
  console.log("Fetching DB data from: ", req.params.id)
	Doc.findOne({p2bId: req.params.id}, function(err, result){
		if(err)	console.log(err)
		else	res.json(result)
	})
})

// Fetching data from blockchain
router.get('/api/fetch.blockdata/:id', function (req, res) {
  console.log("Fetching Blockchain data from: ", req.params.id)
  request('http://172.104.170.157:3000/api/certificate/'+req.params.id, function (rerr, rres, rbody){
		if (rerr) console.log(rerr);
		else res.json(JSON.parse(rbody))
		//console.log(rody);
	})
})

// Fetching Document
router.get('/api/fetch.file/:id', function (req, res) {
	var filename = Math.round(Math.random()*1000000000)
	storj.resolveFile(bucketId, req.params.id, './temp/'+filename, {
		progressCallback: function(progress, downloadedBytes, totalBytes) {},
		finishedCallback: function(err) {
			if (err) {
				console.log(err)
				res.json({'error':'No file'})
			}
			else res.json({filename: filename})
	}})
})

// Generating Hash and Comparing
router.get('/api/hash/:id', function (req, res) {
	fs.stat('./temp/'+req.params.id, function (err, stats) {
		if (err) res.json(err);
		else
			hashFiles({files: './temp/'+req.params.id, algorithm: 'sha256'}, function(herr, hash) {
				if (herr) console.log(herr);
				else res.json({hash: hash})
			})
	})
})

// Get univ publickey
router.get('/api/univ.pubkey/:id', function (req, res) {
  Issuer.findById(req.params.id, {name: 1, pubkey: 1, isRSA: 1}, function(err, univ){
    if (err)  res.json(err);
    else res.json(univ)
  })
})

// //Verifying Cryptographic Signatures
router.post('/api/verify/', function (req, res) {
	console.log(req.body)
	verifySign(req.body.pubkey, req.body.hash, req.body.encHash, req.body.isRSA, function(isValid){
		console.log(isValid)
		res.json({'verified': isValid})
	})
})

// revocation status
router.get('/api/revocation/:id', function (req, res) {
	res.json({'status':true})
})

// //More student info
router.get('/api/student.info/:id', function (req, res) {
  console.log("Getting student data")
	Student.findById(req.params.id, function(err, info){
		if (err) console.log(err)
		else	
			res.json(info)
	})
})