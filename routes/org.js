const express = require('express');
const router = express.Router();

var Issuer = require('../models/issuer')
var Univ = require('../models/univ')
var Customer = require('../models/customer')
var authsys = require('../cjs/authsys')
var Doc = require('../models/doc')
const multer  = require('multer');
var upload = multer({ dest: 'public/uploads/' })

const fs  = require('fs');
const hashFiles = require('hash-files');
const request = require('request');
const SHA256 = require("crypto-js/sha256");
const jsrsaSign = require('jsrsasign');
var ipfsAPI = require('ipfs-api')
//
/*********IPFS CONFIG ******/
var ipfs = ipfsAPI('/ip4/127.0.0.1/tcp/5001')

/********* Routes **********/
router.get('/new', authsys.ensureAuthenticated, function(req, res){
  res.locals.user = req.user
  Univ.findOne({dcid: req.user.id}, function(err, result){
    console.log(result)
    req.session['univ'] = result
    //console.log('req.session.univ: ', req.session['univ'])
    if(req.user.pubkey == undefined || req.user.pubkey == null){
      req.flash('error', 'This account does not have a public key. Only accounts with public key can publish documents. Please contact support@docchain.io')
      res.redirect('/user')
    }
    else{
      res.render('org/new',{layout: 'org.hbs', title: 'Search Customer', data: result, phead: 'Search Customer', icon: 'file-o'})
    }
  })
})

router.get('/customer', authsys.ensureAuthenticated, function(req, res){
  console.log(req.query.id)
  console.log('enroll: ', req.query.enroll)
  console.log('show: ', req.query.show)
  Customer.findById(req.query.id, function(err, result){
    if(err){
      console.log(err)
      req.flash('error', err)
      res.redirect('/user')
    }
    else{
      //console.log(result)
      if(result){
        res.locals.user = req.user
        if (req.query.enroll == 'true')
          enroll = true
        else
          enroll = false
        if (req.query.show == 'true')
          show = true
        else
          show = false
        res.render('org/profile',{layout: 'org.hbs', title: 'Customer Profile', data: result, phead: 'Customer Profile', icon: 'user-circle-o', enroll: enroll, show: show})
      }
      else{
        req.flash('error', 'The Customer does not exist')
        res.redirect('/org/customer/enroll')
      }
    }
  })
})

router.get('/customer/add', authsys.ensureAuthenticated, function(req, res){
  res.locals.user = req.user;
  res.render('org/add',{layout: 'org.hbs', title: 'Add Customer', phead: 'Add Customer', icon: 'user-plus'})
})

router.post('/customer/add', upload.single('fsub'), authsys.ensureAuthenticated, function(req, res){
  //console.log('Given value', req.body)
  //console.log('File Attributes', req.file)
  var filename = req.body.sfname +'_'+ req.body.slname +'_'+ Date.now();
  
  var data = {};
  data.student = {};
  data.father = {};
  if(req.body.sfname) data.student.fname = req.body.sfname
  if(req.body.smname) data.student.mname = req.body.smname
  if(req.body.slname) data.student.lname = req.body.slname
  if(req.body.ffname) data.father.fname = req.body.ffname
  if(req.body.fmname) data.father.mname = req.body.fmname
  if(req.body.flname) data.father.lname = req.body.flname
  if(req.body.dob) data.dob = req.body.dob
  if(req.body.email) data.email = req.body.email
  if(req.body.phoneno) data.phoneno = req.body.phoneno
  //console.log(data)

  Customer.doesStudentExist(data, function(err, exist){
    if(err) console.log(err)
    else if (exist){
      req.flash('error', 'The Customer already exists!')
      res.redirect('/org/customer?id='+exist+'&enroll=false&show=false')
      fs.unlink('./public/uploads/'+req.file.filename, (err) => {
        if (err) throw err;
        console.log(req.file.filename + ' was deleted');
      });
    }
    else{
      fs.rename('./public/uploads/'+req.file.filename, './public/customers/'+ filename, (err) => {
        if (err) throw err;
        console.log('Rename complete!');
      }); 
      
      // updataing data object
      data.pic = filename
      // finalising data object for student creation
      data = new Customer(data)
      // creating student object in DB
      Customer.createStudent(data, function(err, student){
        if(err) {
          console.log(err)
          req.flash('error', err)
					res.redirect('/org/customer/add')
        }
        else{
          console.log(student)
          req.flash('success', 'An customer account has been successfully created.')
		      res.redirect('/org/customer/add')
        }
      })
    }
  })
})

/*
router.get('/customer/enroll', authsys.ensureAuthenticated, function(req, res){
  res.locals.user = req.user;
  res.render('org/enroll_id',{layout: 'org.hbs', title: 'Enroll Customer', phead: 'Enroll Customer', icon: 'user-plus'})
})

router.get('/customer/enroll/:id', authsys.ensureAuthenticated, function(req, res){
  console.log(req.params.id)
  res.locals.user = req.user
  Univ.findOne({dcid: req.user.id}, function(err, result){
    result.studid = req.params.id
    //console.log("Result after modify: ", result)
    res.render('org/enroll',{layout: 'org.hbs', title: 'Enroll Customer', phead: 'Enroll Customer', icon: 'user-plus', data: result})
  })
})

router.post('/customer/enroll/:id', authsys.ensureAuthenticated, function(req, res){
  console.log("Body Parametrs: ", req.body)
  req.body.id = req.params.id;
  req.body.univ = req.user.name;
  req.session.course = req.body;
  res.locals.user = req.user;
  console.log("session course: ", req.session.course)
  Customer.findById(req.params.id, function(err, result){
    res.render('org/sprev',{layout: 'org.hbs', title: 'Confirm Customer Enrollment', phead: 'Confirm for Enrollment', icon: 'user-plus', data: result, course: req.body})
  })  
})

router.get('/customer/enroll.course', authsys.ensureAuthenticated, function(req, res){
  console.log("session course: ", req.session.course)
  Customer.joinCourse(req.session.course, req.user.name, function(err, result){
    delete req.session.course
    if(err && err != 'exist'){
      console.log(err)
      req.flash('error', err)
      if(req.session.add){
        req.session.add = false
        res.redirect('/org/customer/add')
      }
      else
        res.redirect('/org/customer/enroll')
    }
    else if(err == 'exist'){
      req.flash('error', 'The Student is already enrolled for this course')
      res.redirect('/org/customer/enroll')
    }
    else{
      req.flash('success', 'The Customer is successfully enrolled')
      //res.send(result)
      if(req.session.add){
        req.session.add = false
        res.redirect('/org/customer/add')
      }
      else
        res.redirect('/org/customer/enroll')
    }
  })
})
*/

router.get('/session.clear/course', authsys.ensureAuthenticated, function(req, res){
  delete req.session.course
  //console.log("session after course delete: ", req.session)
  res.redirect('/org/new')
})

router.post('/customer/list', authsys.ensureAuthenticated, function(req, res){
  // console.log(req.body)
  Customer.findStudent(req.body, req.user.name, function(err, result){
    if(err){
      console.log(err)
      req.flash('error', err)
    }
    if(result.length){
      if(req.body.regid){
        // console.log(result)
        //res.send(result)
        res.redirect('/org/customer/' + result[0].id)
      }
      else{
        //res.send(result)
        res.locals.user = req.user
        res.render('org/list',{layout: 'org.hbs', title: 'Search Result', phead: 'Customer List', icon: 'list', data: result})
      }
    }
    else{
      req.flash('error', 'Found no customers matching criteria')
      res.redirect('/org/customer')
    }
  })
})

router.post('/updoc', authsys.ensureAuthenticated, function(req, res){
  console.log('body parameters', req.body)
  Customer.findStudent(req.body, req.user.name, function(err, result){
    if(err){
      console.log(err)
      req.flash('error', "ERROR IN DB")
      return res.redirect('/org/new')
    }
    if(result.length)
			if(req.body.id){
				req.session.client = result[0]
				//console.log('Session values: req.session[stud]: ', req.session[stud])
				res.locals.user = req.user
				res.render('org/updoc',{layout: 'org.hbs', title: 'Upload Document', phead: 'Upload Document To The Selected Customers', icon: 'file-o', data: result[0]})
			}
			else{
				//res.send(result)
				res.locals.user = req.user
				res.render('org/list',{layout: 'org.hbs', title: 'Search Result', phead: 'Customer List', icon: 'list', data: result})
			}
		else {
      req.flash('error', 'Found no Customers matching criteria')
      res.redirect('/org/new')
    }
    
  })
})

router.get('/docdetail', authsys.ensureAuthenticated, function(req, res){
  //console.log('Student id: ', req.params.id)
  //console.log('Session data: ', req.session)
  res.locals.user = req.user
  //console.log('Session values: ', req.session.client)
  if(req.session.client)
    Univ.findOne({dcid: req.user.id}, function(err, result){
      if(err) console.log(err)
      else
        res.render('org/docdetail',{layout: 'org.hbs', title: 'Select Document', phead: 'Select Document', icon: 'file-o', data: req.session.client, univ: result})
    })
  else
    res.redirect('/org/new')
})

router.post('/docid.check', authsys.ensureAuthenticated, function(req, res){
  console.log("Body parameters: ", req.body)
  Doc.find({udocId: req.body.id}, function(err, result){
    if(err) res.send(false)
    if(result.length==0)
      res.send(true)
    else
      res.send(false)
  })
})

router.post('/pandp', upload.single('fsub'), authsys.ensureAuthenticated, function(req, res, next){
  // console.log('File Attributes', req.file)
  console.log('Body Attributes', req.body)
  if(!req.session.client){
    console.log('Session does not exist')
    res.redirect('/org/new')
  }
  else{
  // Document Session data
  console.log(' Session data: ', req.session.client)
	delete req.session.doc
  var doc = {};
  doc.receiverId = req.session.client._id
  doc.issuer     = req.user.name
  doc.issuerId   = req.user.id
  doc.student    = ''
  if(req.session.client.student.fname)
		doc.student = doc.student + req.session.client.student.fname + ' ' 
	if(req.session.client.student.mname)
		doc.student = doc.student + req.session.client.student.mname + ' ' 
	if(req.session.client.student.lname)
		doc.student = doc.student + req.session.client.student.lname;
  //doc.dob        = req.session[req.params.id].dob;
  //doc.pic        = req.session[req.params.id].pic;
  // Student course info
  doc.to				 = req.session.client.email 
  // Document info
  doc.udocId     = req.body.certId;
  doc.doi        = req.body.idate;
  doc.docType    = req.body.certType;
  doc.filename   = req.session.client.id + '_' +Date.now()
  if(req.body.edate) doc.edate = req.body.edate
  // Univ info
  doc.signatory  = req.body.signatory
  console.log('req.body.signatory: ', req.body.signatory)
  console.log('doc.signatory: ', doc.signatory)

  hashFiles({files: './public/uploads/'+req.file.filename, algorithm: 'sha256'}, function(error, hash){
    if (error)  console.log(error)
    else {
      //console.log(hash)
      doc.hash = hash;
      
      Doc.doesDocExist(hash, function(err, docResult){
        if(err) console.log(err)
        else{
          if(docResult){
            req.flash('error', 'The document already exist!')
            fs.unlink('./public/uploads/'+req.file.filename, (err) => {
              if (err) throw err;
              console.log(req.file.filename + ' was deleted');
            });
            res.redirect('/org/docdetail/')
          }
          else{
            console.log(doc)
            req.session.doc = doc
            var oldpath = './public/uploads/'+req.file.filename
            var newpath = './public/'+ req.user.id
            res.locals.user = req.user
            res.render('org/pandp', {layout: 'org.hbs', title: 'Preview', phead: 'Preview and Sign', icon: 'file-o', data: req.session.client, doc: doc, user: req.user});  
            fs.stat('./public/'+ req.user.id, function(err, stat){
              if(err){
                if(err.code == 'ENOENT')
                  fs.mkdir(newpath, 0o777, function(err){
                    if(err) console.log(err)
                    else
                      fs.rename(oldpath, newpath +'/'+ doc.filename, (err) => {
                        if (err) throw err;
                        console.log('file move complete!');
                      });
                  })
              }
              else
                fs.rename(oldpath, newpath +'/'+ doc.filename, (err) => {
                  if (err) throw err;
                  console.log('file move complete!');
                });
            })
          }
        }
      })
    }
  })
  }
})

router.post('/confirm/', authsys.ensureAuthenticated, function(req, res){
	console.log(req.body)
  req.session.doc.hashSign = req.body.signature;
  var data = {};
  data = req.session.doc;
  //var sign = req.body.signature
	data.txId = SHA256('TXID'+req.body.signature).toString();
  data.p2bId = SHA256(req.body.signature).toString();
  
  //console.log("txid: ", data.txId)
  //console.log("p2bId: ", data.p2bId)
  
  req.session.doc.txId = data.txId
  req.session.doc.p2bId = data.p2bId
  
  console.log("session: ", req.session.doc)
  
  data.hostname = req.hostname
  data.from = req.user.email 
  console.log("Hostname: ", req.hostname)
  Issuer.removeOneSlot(req.user.email, function(uerr, hasSlot){
    if(uerr && uerr != 'noSlot'){
      console.log(uerr)
    }
    if(uerr == 'noSlot'){
      req.flash('error', "You have 0 slot. Please purchase more to upload documents.")
			res.redirect('/org/new')
    }
    if(hasSlot){
      req.session.doc.token = true
      res.location('/org/new')
      res.locals.user = req.user
			res.render('org/confirm', {layout: 'org.hbs', title: 'Confirm', icon: 'file-o', phead: 'Publish Document', data: data})
		}
	})
})

//Verifing Issuer Domain
router.get('/confirm.domain/', authsys.ensureAuthenticated, function(req, res){
	console.log("Request Domain: ", req.hostname)
	if(req.session.doc.token = true)
		res.json({"status": true, "msg": null})
	else
		res.json({"status": false, "msg": "Token doesn't exist"})
})

//Generating Document Hash
router.get('/confirm.hash/', authsys.ensureAuthenticated, function(req, res){
	if(req.session.doc.token = true){
		var filePath = './public/'+ req.session.doc.issuerId + '/' + req.session.doc.filename;
		hashFiles({files: filePath, algorithm: 'sha256'}, function(error, hash){
			if(error){
				console.log(error)
				res.json({"status": false, "msg": error})
			}
			else{
				console.log("hash : "+hash+" Session hash : "+req.session.doc.hash)
				if(hash == req.session.doc.hash)
					res.json({"status": true, "msg": null})
				else{
					req.session.doc.token = false
					res.json({"status": false, "msg": "Hash integrity failed"})
				}
			}
		})		
	}
	else
		res.json({"status": false, "msg": "Token doesn't exist"})
})

//Verifing Issuer Signature
router.get('/confirm.signature/', authsys.ensureAuthenticated, function(req, res){
	console.log("Pubkey: ", req.user.pubkey)
	console.log("Hash: ", req.session.doc.hash)
	console.log("Signature: ", req.session.doc.hashSign)
	console.log("isRSA: ", req.user.isRSA)
	if(req.session.doc.token = true){
		verifySign(req.user.pubkey, req.session.doc.hash, req.session.doc.hashSign, req.user.isRSA, function(isValid){
			console.log(isValid)
			if(isValid){
				//req.session.doc.txId = SHA256('TXID'+req.session.doc.hashSign);
				res.json({"status": true, "msg": null})
			}
			else{
				req.session.doc.token = false
				res.json({"status": false, "msg": "Signature verification failed"})
			}
		})
	}
	else
		res.json({"status": false, "msg": "Token doesn't exist"})
})

//Uploading file to decentralised storage
router.get('/confirm.file/', authsys.ensureAuthenticated, function(req, res){
	console.log(req.session.doc.filename)
	if(req.session.doc.token = true){
		var filePath = './public/'+ req.session.doc.issuerId + '/' + req.session.doc.filename;/*
		const state = storj.storeFile(bucketId, filePath, {
			filename: req.session.doc.filename,
			progressCallback: function(progress, downloadedBytes, totalBytes) {
			//console.log('progress:', progress);
			},
			finishedCallback: function(err, fileId) {
				if (err) {
					console.error(err);
					req.session.doc.token = false
					res.json({"status": false, "msg": err})
				} 
				else	{
					console.log("storj fileid: ", fileId)
					req.session.doc.storjfileid = fileId
					res.json({"status": true, "msg": null})
				}
			}
    })*/
    var readStream = fs.createReadStream(filePath);

    var file = [{
      path: req.session.doc.filename,
      content: readStream
    }]

    ipfs.files.add(file, function (err, files) {
      if (err){
        console.error(err);
        req.session.doc.token = false
        res.json({"status": false, "msg": "IPFS is encountering connection issues."})
      }
      else{
        console.log("IPFS fileid: ", files['0'].hash)
        req.session.doc.storjfileid = files['0'].hash
        res.json({"status": true, "msg": null})
      }
    })    
	}
	else
		res.json({"status": false, "msg": "Token doesn't exist"})
})

//Publishing On Blockchain
router.get('/confirm.blockchain/', authsys.ensureAuthenticated, function(req, res){
	console.log(req.session.doc)
	if(req.session.doc.token = true){
		request({
			url: 'http://34.93.244.135:8000/invoke',
			method: 'POST',
			json: {
        "channel"   : "mychannel",
        "chaincode" : "docchain",
        "function"  : "issueDocument",
        "args"      : [req.session.doc.p2bId, req.session.doc.hash, req.session.doc.hashSign, req.session.doc.storjfileid, req.session.doc.issuerId, req.session.doc.receiverId, "metadata"]
			}
		}, function(rerr, rres, rbody){
			//console.log(rres)
			console.log(rbody)
			if(rbody.status == 'OK')
				res.json({"status": true, "msg": null})
			else{
				req.session.doc.token = false
				res.json({"status": false, "msg": "Publishing to Blockchain failed"})
			}
		})
	}
	else
		res.json({"status": false, "msg": "Token doesn't exist"})
})

//Generating True Copy Link
router.get('/confirm.link/', authsys.ensureAuthenticated, function(req, res){
	req.session.doc.p2bId = SHA256(req.session.doc.hashSign)
	
	if(req.session.doc.token = true){
		Doc.findOneAndUpdate({p2bId: req.session.doc.p2bId}, req.session.doc, { "new": true, "upsert": true }, function(err, doc){
			if(err)	{
				console.log(err)
				res.json({"status": false, "msg": err})
			}
			else{
				req.session.doc.token = false
				res.json({"status": true, "msg": null})
			}
		})
	}
	else
		res.json({"status": false, "msg": "Token doesn't exist"})
})

router.post('/update.certificate/', authsys.ensureAuthenticated, function(req, res){
  console.log('Data: ', req.body)
  Doc.findOneAndUpdate({p2bId: req.body.p2bid},{$set:{PAtxId: req.body.paid, PAleafId: req.body.id}}, {new: true}, function(err, result){
    if(err){
      console.log(err)
      res.send('FAILED')
    } else{
      console.log(result)
      if(result.PAtxId == req.body.paid){
        res.send('SUCCESS')
      }
    }
  })
})

router.get('/certificate/:id', function(req, res){
  console.log(req.params.id);
  Doc.findOne({p2bId: req.params.id}, function(err, result){
		console.log('result', result)
    res.render('org/certificate', {data: result, title: 'Certificate', verify: false})
  })
})

router.post('/certificate/:id', function (req, res) {
  var stat=0;
  // Fetching encrypted digest from blockchain
  Doc.findOne({p2bId: req.params.id}, function(err, result){
    console.log(result)
    request({
			url: 'http://34.93.244.135:8000/query',
			method: 'POST',
			json: {
        "channel"   : "mychannel",
        "chaincode" : "docchain",
        "function"  : "queryDocument",
        "args"      : [result.p2bId]
			}
		}, function(reqErr, reqRes, reqBody){
			if (reqErr) console.log(reqErr);
			else{
        console.log('Encrypted hash obtained: ', reqBody);
				stat+=1;
        //reqBody = JSON.parse(reqBody);
        //console.log(reqBody)
        reqBody =  JSON.parse(reqBody.response)
				encHash=reqBody.signature;
				console.log('Encrypted hash: ', encHash);
				//certId=reqBody.certId;
				//console.log('Certificate ID: ', certId);
				var filePath = './'+result.filename;
				console.log('storjId : ', result.storjfileid)
        // Fetching document from storage
        
        ipfs.files.get(result.storjfileid, function (err, files) {
          files.forEach((file) => {
            //console.log(file)
            var wstream = fs.createWriteStream(filePath);
            wstream.write(file.content);
            wstream.end();
            wstream.on('finish', function () {
              console.log('File has been downloaded');
              stat+=1;
              // Generating digest of the document
              hashFiles({files: filePath, algorithm: 'sha256'}, function(hashErr, hash) {
                // Clearing tempfile
                fs.stat(filePath, function (err, stats) {
                  //console.log(stats);//here we got all information of file in stats variable
                  if (err) console.error(err);
                  else
                    fs.unlink(filePath, function(err){
                      if(err) console.log(err);
                      console.log('file deleted successfully');
                    });
                });
                
                if(hashErr) console.log('file hash: error: ', hashErr);
                else{
                  console.log('Hash succeded', hash);
                  stat+=1;
                  
                  // Verifying signatures
                  console.log('IssuerId: ', result.issuerId);
                  Issuer.findById(result.issuerId, function(uDBerr, uDBres){
                    if (uDBerr) console.log(uDBerr)
                    else{
                      //console.log('Issuer DB Data: ', uDBres);
                      if(uDBres.pubkey == null || uDBres.pubkey == undefined){
                        req.flash('error', 'PUBLIC KEY is not found')
                        res.redirect('/org/certificate/'+req.params.id)
                      }
                      else
                      verifySign(uDBres.pubkey, hash, encHash, uDBres.isRSA, function(isValid){
                        console.log(isValid)
                        if (isValid){
                          stat+=1			
                        }
                        // Checking revocation status
                        stat+=1;
                        console.log(stat);
                        console.log(result.receiverId);
                        Customer.findById(result.receiverId, function(err, info){
                          if (err) console.log(err)
                          console.log('info: ', info)
                          res.render('org/certificate', {data: result, title: 'Certificate', stat: stat, info: info, verify: true})
                        })
                      })
                    }
                  })
                }
              });
            });
          })
        })
			}
    });
  });
});

router.get('/settings', authsys.ensureAuthenticated, function(req, res){
  Univ.findOne({dcid: req.user.id}, function(err, result){
    res.locals.user = req.user
    res.render('org/set', {layout: 'org.hbs', title: 'Settings', data: result, phead: 'Account Setting', icon: 'cogs'}); 
  })
})

router.post('/settings', authsys.ensureAuthenticated, function(req, res){
  console.log('Body parameters: ', req.body)
  var update = {}
  var msg;
  if(req.body.docType)
    {
      update.docType = req.body.docType
      msg = req.body.docType+' has been added to "Type of Document"'
    }
  if(req.body.signatory) 
  {
    update.signatory = req.body.signatory
    msg = req.body.signatory+' has been added to "Authorized Signatory"'  
  }
  if(req.body.docType && req.body.signatory)
    msg = '"'+req.body.signatory+'" & "'+req.body.docType+'" has been added to "Authorized Signatory" and "Type of Document" respectively'  
  req.flash('success', msg)
  Univ.findOneAndUpdate({dcid: req.user.id}, {$addToSet: update}, { "new": true, "upsert": true }, function(err, result){
    if(err) console.log(err)
    else{
      res.redirect('/org/settings') 
    }
  })
})

router.post('/settings.signatory', authsys.ensureAuthenticated, function(req, res){
  console.log('Body parameters: ', req.body)
  var update = {}
  update.signDetail = req.body
  console.log('Update value', update)
  Univ.findOneAndUpdate({dcid: req.user.id}, {$pull: { 'signDetail': {  docType: req.body.docType }}}, function(err, result){
    if(err) console.log(err)
    else{
      //console.log("After Entry Removal", result)
      Univ.findOneAndUpdate({dcid: req.user.id}, {$addToSet: update}, { "new": true, "upsert": true }, function(err, result){
        if(err) console.log(err)
        else{
          //console.log('after signdetails update', result)
          req.flash('success', '"'+req.body.docType+'" is linked with "'+req.body.signatory+'"')
          res.redirect('/org/settings')  
        }
      })
    }
  })
})

router.get('/settings.remove.signatory', authsys.ensureAuthenticated, function(req, res){
  console.log('Doctype-Signatory to be removed : ', req.query.docType)
  Univ.findOneAndUpdate({dcid: req.user.id}, {$pull: { 'signDetail': {  docType: req.query.docType }}}, function(err, result){
    if(err) console.log(err)
    else{
      req.flash('error', req.query.docType+" has been removed from the list of Authorised Signatories")
			res.redirect('/org/settings')
    }
  })
})



router.post('/doc.id.check', authsys.ensureAuthenticated, function(req, res){
  console.log("Body parameters: ", req.body)
  Doc.find({udocId: req.body.id}, function(err, result){
    if(err) res.send(false)
    if(result.length==0)
      res.send('Message : DocId is available for use, id : '+req.body.id)
    else
      res.send('Message : Docid is taken, Action : Try a new one')
  })
})

router.post('/file.add', upload.single('fsub'), authsys.ensureAuthenticated, function(req, res){
  console.log("String to process file")
  //console.log("Body parameters: ", req.body)
  console.log('File Attributes', req.file)
  var data = {}
  hashFiles({files: './public/uploads/'+req.file.filename, algorithm: 'sha256'}, function(error, hash){
    if (error)  
      res.json({'message': 'ERROR:: Hashing failed', 'error': error, 'action': 'Try again or contact support'})
    else {
      console.log(hash)
      data.hash = hash;
      Doc.doesDocExist(hash, function(err, docResult){
        if(err) 
          res.json({'message': 'ERROR:: Filecheck failed', 'error': err, 'action': 'Try again or contact support'})
        else{
          if(docResult){
            res.json({'message': 'File exist', 'action': 'Send new file or contact support'})
            fs.unlink('./public/uploads/'+req.file.filename, (err) => {
              if (err) throw err;
              console.log(req.file.filename + ' was deleted');
            });
          }
          else{
            var oldpath = './public/uploads/'+req.file.filename
            var newpath = './public/'+ req.user.id
            data.path = newpath
            data.hash = hash
            data.filename = req.user.id + '_' + Date.now()
            fs.stat('./public/'+ req.user.id, function(err1, stat){
              if(err1){
                if(err1.code == 'ENOENT')
                  fs.mkdir(newpath, 0o777, function(err2){
                    if(err2) res.json({'message': 'ERROR:: create directory failed', 'error': err2, 'action': 'Try again or contact support'})
                    else
                    fs.rename(oldpath, newpath +'/'+ data.filename, (err3) => {
                      if (err3) res.json({'message': 'ERROR:: File move failed', 'error': err3, 'action': 'Try again or contact support'});
                      console.log('file move complete!');
                      res.json({'message': 'File uploaded sucessfuly', 'file': data})
                    });
                  })
              }
              else
                fs.rename(oldpath, newpath +'/'+ data.filename, (err4) => {
                  if (err4) res.json({'message': 'File uploaded sucessfuly', 'file': data });
                    console.log('file move complete!');
		    res.json({'message': 'File uploaded sucessfuly', 'file': data})
                });
            })
          }
        }
      })
    }
  })
})

router.post('/doc.init', authsys.ensureAuthenticated, function(req, res){
  // console.log('File Attributes', req.file)
  console.log('Body Attributes', req.body)
  // Document Session data
  delete req.session.doc
  console.log("Deleting previous doc objects")
  var doc = {};
  doc.issuer        = req.user.name
  doc.issuerId      = req.user.id
  if(req.body.dob)
    doc.dob           = req.body.dob;
  if(req.body.pic)
    doc.pic           = req.body.pic;
  if(req.body.receiverId)
    doc.receiverId    = req.body.receiverId
  if(req.body.email)
    doc.email = req.body.email
  if(req.body.receiver){
    doc.student = {}
    if(req.body.receiver.fname)
      doc.student.fname = req.body.receiver.fname
    if(req.body.receiver.mname)
      doc.student.mname = req.body.receiver.mname
    if(req.body.receiver.lname)
      doc.student.sname = req.body.receiver.lname
  }
  // Document info
  doc.udocId          = req.body.docid;
  doc.doi             = req.body.idate;
  if(req.body.docType)
    doc.docType       = req.body.docType;
  doc.filename        = req.body.file
  doc.hash            = req.body.hash
  if(req.body.edate) doc.edate = req.body.edate
  // Univ info
  doc.signatory  = req.body.signatory
  console.log('req.body.signatory: ', req.body.signatory)
  console.log('doc.signatory: ', doc.signatory)
  req.session.doc = doc
  res.json({'message': 'Document object initiated', 'document': doc})
})

router.post('/doc.update', authsys.ensureAuthenticated, function(req, res){
  if(req.session.doc == '')
    res.json({'message': 'ERROR::Document object not created', 'action': 'Run /doc.init endpoint'})
  else{
    req.session.doc.hashSign = req.body.signature
    req.session.doc.txId = SHA256('TXID'+req.body.signature).toString();
    req.session.doc.p2bId = SHA256(req.body.signature).toString();
    res.json({'message':'Document object updated', 'document': req.session.doc})
  }
})

router.get('/doc.publish', authsys.ensureAuthenticated, function(req, res){
  Issuer.removeOneSlot(req.user.email, function(uerr, hasSlot){
    if(uerr && uerr != 'noSlot'){
      console.log(uerr)
      res.json({'message': 'ERROR:: DB Error', 'error': uerr, 'action': 'Try again or contact support'})
    }
    if(uerr == 'noSlot'){
      res.json({'message': 'ERROR:: You have 0 upload slot', 'action': 'Buy more slots or contact support'})
    }
    if(hasSlot){
      req.session.doc.token = true
      request('http://172.104.170.157:9000/org/confirm.domain', function(err1, res1, body1) { 
        if(err1)
          res.json({'message': 'ERROR:: in domain confirm ', 'error': err1, 'action': 'Try again or contact support'})
        else
          if(body1.status)
            request('http://172.104.170.157:9000/org/confirm.hash', function(err2, res2, body2) { 
              if(err2)
                res.json({'message': 'ERROR:: in hash confirm ', 'error': err2, 'action': 'Try again or contact support'})
              else
                if(body2.status)
                  request('http://172.104.170.157:9000/org/confirm.signature', function(err3, res3, body3) { 
                    if(err3)
                      res.json({'message': 'ERROR:: in signature confirm ', 'error': err3, 'action': 'Try again or contact support'})
                    else
                      if(body3.status)
                        request('http://172.104.170.157:9000/org/confirm.file', function(err4, res4, body4) { 
                          if(err4)
                            res.json({'message': 'ERROR:: in file confirm ', 'error': err4, 'action': 'Try again or contact support'})
                          else
                            if(body4.status)
                              request('http://172.104.170.157:9000/org/confirm.blockchain', function(err5, res5, body5) { 
                                if(err5)
                                  res.json({'message': 'ERROR:: in blockchain confirm ', 'error': err5, 'action': 'Try again or contact support'})
                                else
                                  if(body5.status)
                                  request('http://172.104.170.157:9000/org/confirm.link', function(err6, res6, body6) { 
                                    if(err6)
                                      res.json({'message': 'ERROR:: in blockchain confirm ', 'error': err6, 'action': 'Try again or contact support'})
                                    else
                                      if(body6.status)
                                        res.json({'message': 'Document successfully published', 'url':'http://172.104.170.157:9000/org/certificate/'+req.session.doc.p2bId})
                                      else
                                        res.json({'message': 'ERROR:: Error in domain confirmation', 'error': body6.msg, 'action': 'Try again or contact support'})
                                  })
                                  else
                                    res.json({'message': 'ERROR:: Error in domain confirmation', 'error': body5.msg, 'action': 'Try again or contact support'})
                              })
                            else
                              res.json({'message': 'ERROR:: Error in domain confirmation', 'error': body4.msg, 'action': 'Try again or contact support'})
                        })
                      else
                        res.json({'message': 'ERROR:: Error in domain confirmation', 'error': body3.msg, 'action': 'Try again or contact support'})
                  })
                else
                  res.json({'message': 'ERROR:: Error in domain confirmation', 'error': body2.msg, 'action': 'Try again or contact support'})
            })
          else
            res.json({'message': 'ERROR:: Error in domain confirmation', 'error': body1.msg, 'action': 'Try again or contact support'})
      })
    }
  })
})




module.exports = router;

/********** CUSTOM FUNCTIONS ***********/
var verifySign = function(pubkey, msg, signature, isRSA, callback){
  var isValid;
  if(isRSA){
    var sig = new jsrsaSign.crypto.Signature({"alg": "SHA256withRSA"});
    // initialize for signature validation
    sig.init(pubkey); // signer's certificate or pubkey
    // update data or msg
    sig.updateString(msg)
    // verify signature
    isValid = sig.verify(signature)
  }
  else{
    var sig = new jsrsaSign.crypto.Signature({"alg": 'SHA256withECDSA', "prov": "cryptojs/jsrsa"});
    sig.init({xy: pubkey, curve: 'secp256r1'});
    sig.updateString(msg);
    isValid = sig.verify(signature);
  }
  callback(isValid);
}

/******* Test routes and data *******/
/*
router.get('/confirm/', authsys.ensureAuthenticated, function(req, res){
  var docdata ={};
  docdata.udocId = 'testcert003'
  docdata.hashSign = '1234567899gr8r4g89g494g68g1g46g4seg4s'
  docdata.to = 'priyavenkat16295@gmail.com'
  docdata.from = req.user.email
  docdata.issuer = req.user.name
  //docdata.subject = 'Your "'+ docdata.udocId +'" from ' + docdata.issuer
  res.render('univ/confirm', {layout: 'univ.hbs', data: docdata})
})
*/
