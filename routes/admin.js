const express = require('express');
const router = express.Router();

var Issuer = require('../models/issuer');
var authSys = require('../cjs/authsys')

/********* Routes **********/

//authSys.ensureAuthenticated, authSys.isAdmin, 

router.get('/', authSys.ensureAuthenticated, authSys.isAdmin, function(req, res){
    //console.log(req.user)
    res.render('admin/dashboard', {layout: 'admin.hbs', title: 'Admin - Dashboard', phead: 'Dashboard', icon: 'dashboard'})
})

router.get('/user', authSys.ensureAuthenticated, authSys.isAdmin, function(req, res){
    //console.log(req.user)
    res.render('admin/user', {layout: 'admin.hbs', title: 'Admin'})
})

router.get('/user/add', authSys.ensureAuthenticated, authSys.isAdmin, function(req, res){
    // console.log(req.user)
    res.render('admin/register', {layout: 'admin.hbs', title: 'Admin', phead: 'Add Organisation', icon: 'plus-circle'})
})

router.post('/user/add', authSys.ensureAuthenticated, authSys.isAdmin, function(req, res){
    // console.log(req.body);
    // res.render('admin/register', {layout: 'homeLayout.hbs'})
    var email = req.body.email;
    
    // Form Validation
    req.checkBody('email', 'Enter a VALID email').isEmail();
    var err = req.validationErrors();

    if (err){
        console.log(err);
        res.render('admin/register', {layout: 'admin.hbs', title: 'Admin', error: err, phead: 'Add Organisation', icon: 'plus-circle'})
    }    
    else {
        req.flash('success', 'New User Profile has been created.')
        res.location('/');
        res.redirect('/admin');

        // preparing data for issuer object creation
        req.body.docs = 0;
        if(req.body.edu) req.body.docs += 10;
        if(req.body.org) req.body.docs += 10;
        if(req.body.eve) req.body.docs += 10;

        if(req.body.edu && !req.body.org && !req.body.eve) {
            console.log('Setting default model: univ.hbs')
            req.body.model = 'univ.hbs'
        }
        
        if(!req.body.edu && req.body.org && !req.body.eve) {
            console.log('Setting default model: org.hbs')
            req.body.model = 'org.hbs'
        }

        //console.log('After updating body parameters: ', req.body)

        // finalising data for object creation
        var newIssuer = new Issuer(req.body);

        // Create issuer object in DB
        Issuer.createIssuer(newIssuer, function(err, issuer){
            if (err) throw err;
            else {
                //console.log(issuer);
                Issuer.createAppData(issuer)
            }
        })
    }    
})

router.post('/check.email', authSys.ensureAuthenticated, authSys.isAdmin, function(req, res){
    //console.log("Body parameters: ", req.body)
    Issuer.find({email: req.body.email}, function(err, result){
        if(err) res.send(false)
        //console.log('Result: ', result)
        if(result.length==0)
            res.send(true)
        else
            res.send(false)
    })
})

router.post('/check.name', authSys.ensureAuthenticated, authSys.isAdmin, function(req, res){
    //console.log("Body parameters: ", req.body)
    Issuer.find({name: req.body.name}, function(err, result){
        if(err) res.send(false)
        //console.log('Result: ', result)
        if(result.length==0)
            res.send(true)
        else
            res.send(false)
    })
})

router.get('/user/list', authSys.ensureAuthenticated, authSys.isAdmin, function(req, res){
    // console.log(req.user)
    Issuer.getUserList(function(err, users){
        // console.log(users)
        res.render('admin/list', {layout: 'admin.hbs', title: 'List Organisation', user: users, phead: 'List of Organisations', icon: 'list'})
    })
})

router.get('/user/profile', authSys.ensureAuthenticated, authSys.isAdmin, function(req, res){
    // console.log(req.query.email)
    Issuer.getUserByEmail(req.query.email, function(err, data){
        if (err) console.log('Error: ', err)
        else{
            // console.log(data)
            if(data==null){
                console.log('user account does not exist')
                req.flash('error','User account does not exist.')
                res.redirect('/admin')
            }
            else if(data.accType=='user'){
                res.render('admin/profile', {layout: 'admin.hbs', title: 'Update organisation', data: data, phead: 'Update Organisation', icon: 'pencil-square-o'})
            }
            else{
                console.log('Not an user account')
                req.flash('error','Not an user account')
                res.redirect('/admin')
            }
        }
    })
})

router.post('/user/profile', authSys.ensureAuthenticated, authSys.isAdmin, function(req, res){
    console.log(req.body)
    Issuer.find({pubkey: req.body.pubkey}, function(err, result){
        if(err) res.send(false)
        //console.log('Result: ', result)
        //if(result.length==0)
            Issuer.updatePubkey(req.body, function(err, result){
                if(err == 'noUser'){
                    req.flash('error', 'Unknown User')
                    res.redirect('/admin')
                }
                else if(err == 'inPass')
                    req.flash('error', 'Invalid Password')
                else if(err) console.log(err)
                else
                    req.flash('success','Public Key added Successfuly')
                res.redirect('/admin/user/profile?email='+result.email)
            });/*
        else{
            req.flash('error','Public Key already exists')
            res.redirect('/admin/user/profile?email='+req.body.email)
        }     */       
    })
    
})

router.post('/user/upgrade', authSys.ensureAuthenticated, authSys.isAdmin, function(req, res){
    console.log(req.body)
    if(!req.body.edu) req.body.edu = false
    if(!req.body.org) req.body.org = false
    if(!req.body.eve) req.body.eve = false
    Issuer.upgradeAccount(req.body, function(err, result){
        if(err) {
            console.log(err)
            req.flash('error', 'Something went wrong, error: '+ err)
            res.redirect('/admin')
        }
        else{
            //console.log('Upgrade result: ', result)
            req.flash('success', 'Account upgrade successful')
            res.redirect('/admin/user/profile?email='+result.email)
        }
    })
})

router.post('/user/docs', authSys.ensureAuthenticated, authSys.isAdmin, function(req, res){
    console.log(req.body)
    if(req.body.docs > 0){
        Issuer.addDocSlots(req.body)
        var message =  req.body.docs+' slots were added to account '+ req.body.email +', successfully';
        req.flash('success', message)
    }
    else{
        req.flash('error', 'Value must be greater than 0' )
    }   
    res.redirect('/admin/user/profile?email='+req.body.email)
})
module.exports = router;
