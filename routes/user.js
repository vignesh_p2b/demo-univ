const express = require('express');
const router = express.Router();

var User = require('../models/issuer');
var Univ = require('../models/univ')
var authsys = require('../cjs/authsys')

/********* Routes **********/
router.get('/', authsys.ensureAuthenticated, function(req, res){
  //console.log('/user - req.session: ', req.session)
  // if(req.session.model) layout = req.session.model
  // else layout = 'user.hbs'
  res.locals.user = req.user;
  //console.log('res.locals: ', res.locals)
  res.render('user/dashboard', {layout: req.user.model, title: 'Dashboard - '+req.user.name})
})

router.post('/changepwd', authsys.ensureAuthenticated, function(req, res){
  console.log('user password change: ', req.body)
  User.changepwd(req.user.email, req.body, function(err, msg){
    if(err == 'noUser')
      req.flash('error', 'Unknown User')
    else if(err == 'inPass')
      req.flash('error', 'Invalid Password')
    else if(err) console.log(err)
    else
      if(msg=='ok')
        req.flash('success','Password has been successfully changed')
    res.redirect('/user')
  })
})

router.post('/changemodel',  authsys.ensureAuthenticated, function(req, res){
  //console.log(req.body)
  var model = 'user.hbs'
  if(req.body.model=='edu') 
    if(req.user.edu){
      req.flash('success', 'Educational model loaded successfully and set as default')
      // req.session.model='univ.hbs'
      model = 'univ.hbs'
    }
    else  req.flash('error', 'Educational model is not available for you')

  if(req.body.model=='org') 
    if(req.user.org){
      req.flash('success', 'Organisation model loaded successfully and set as default')
      //req.session.model='user.hbs'
      model = 'org.hbs'
    }
    else  req.flash('error', 'Organisation model is not available for you')

  if(req.body.model=='eve') 
    if(req.user.eve){
      req.flash('success', 'Event model loaded successfully and set as default')
      //req.session.model='user.hbs'
      model = 'event.hbs'
    }
    else  req.flash('error', 'Event model is not available for you')
  //console.log('res.session from change model: ', req.session)

  // Changing new default model
  User.setDefaultModel(req.user.email, model, function(err){
    if (err)
      console.log('User.setDefaultModel ERROR: ', err)
    res.redirect('/user')
  })
})

module.exports = router;

/****scrapped routes 

router.get('/profile', authsys.ensureAuthenticated, function(req, res){
  res.locals.user = req.user;
  Univ.doesUnivExist(req.user.id, function(err, result){
    if(result != null)
      res.render('user/showProfile', {layout: layout, title: 'Profile', data: result})
    else
      res.render('user/profile', {layout: layout, title: 'Create Profile'})
  })
})

router.post('/profile', authsys.ensureAuthenticated, function(req, res){
  //console.log(req.body)
  Univ.doesUnivExist(req.user.id, function(err, result){
    if(result != null){
      console.log('University profile exists')
      res.redirect('/user/profile')
    }
    else{
      res.redirect('/user')
      var newUniv = new Univ(req.body)
      Univ.createUniv(newUniv, function(err, result){
        console.log('University profile created')
        console.log(result)
      })
    }
  })
})

*/