const express = require('express');
const router = express.Router();

const Issuer = require('../models/issuer')
const authsys = require('../cjs/authsys')
const Doc = require('../models/doc')
const Setting = require('../models/setting')
const Customer = require('../models/customer')
const Vendor = require('../models/vendor')
const multer  = require('multer');
const upload = multer({ dest: 'public/uploads/' })

const fs  = require('fs');
const hashFiles = require('hash-files');
const request = require('request');
const SHA256 = require("crypto-js/sha256");
const jsrsaSign = require('jsrsasign');
const ipfsAPI = require('ipfs-api')
const QRCode = require('qrcode')

/********* Routes **********/
router.get('/', authsys.ensureAuthenticated, function(req, res){
  //console.log('/user - req.session: ', req.session)
  // if(req.session.model) layout = req.session.model
  // else layout = 'user.hbs'
  res.locals.user = req.user;
  console.log('res.locals: ', res.locals)
  res.render('invoice/dashboard', {layout: 'org.hbs', title: 'Dashboard - '+req.user.name})
})

router.get('/new', authsys.ensureAuthenticated, function(req, res){
  res.locals.user = req.user
  res.render('invoice/new',{layout: 'org.hbs', title: 'Search Customer', phead: 'Search Customer', icon: 'file-o'})
})

router.get('/customer', authsys.ensureAuthenticated, function(req, res){
  console.log(req.query.id)
  console.log('enroll: ', req.query.enroll)
  console.log('show: ', req.query.show)
  Customer.findById(req.query.id, function(err, result){
    if(err){
      console.log(err)
      req.flash('error', err)
      res.redirect('/user')
    }
    else{
      //console.log(result)
      if(result){
        res.locals.user = req.user
        if (req.query.enroll == 'true')
          enroll = true
        else
          enroll = false
        if (req.query.show == 'true')
          show = true
        else
          show = false
        res.render('invoice/profile',{layout: 'org.hbs', title: 'Customer Profile', data: result, phead: 'Customer Profile', icon: 'user-circle-o', enroll: enroll, show: show})
      }
      else{
        req.flash('error', 'The Customer does not exist')
        res.redirect('/invoice/customer/enroll')
      }
    }
  })
})

router.get('/customer/add', authsys.ensureAuthenticated, function(req, res){
  res.locals.user = req.user;
  res.render('invoice/add',{layout: 'org.hbs', title: 'Add Customer', phead: 'Add Customer', icon: 'user-plus'})
})

router.post('/customer/add', upload.single('fsub'), authsys.ensureAuthenticated, function(req, res){
  console.log('Given value', req.body)
  //console.log('File Attributes', req.file)
  let filename = req.body.name +'_'+ Date.now();
  
  let data = {};
  if(req.body.name) data.name = req.body.name
  if(req.body.email) data.email = req.body.email
  if(req.body.phoneno) data.phoneno = req.body.phoneno
  if(req.body.address) data.address = req.body.address
  //console.log(data)

  Customer.findOne({name: data.name}, function(err, exist){
    if(err) console.log(err)
    else {
      console.log(exist)
      if (exist) {
        req.flash('error', 'The customer already exists!')
        res.redirect('/invoice/customer?id='+exist+'&enroll=false&show=false')
        fs.unlink('./public/uploads/'+req.file.filename, (err) => {
          if (err) throw err;
          console.log(req.file.filename + ' was deleted');
        });
      }
      else{
        fs.rename('./public/uploads/'+req.file.filename, './public/customers/'+ filename, (err) => {
          if (err) throw err;
          console.log('Rename complete!');
        }); 
        
        // updataing data object
        data.logo = filename
        // finalising data object for student creation
        data = new Customer(data)
        // creating student object in DB
        Customer.createCustomer(data, function(err, customer){
          if(err) {
            console.log(err)
            req.flash('error', err)
            res.redirect('/invoice/customer/add')
          }
          else{
            console.log(customer)
            req.flash('success', 'The Customer account has been successfully created.')
            res.redirect('/invoice/customer/add')
          }
        })
      }
    }
  })
})

router.post('/customer/list', authsys.ensureAuthenticated, function(req, res){
  // console.log(req.body)

  Customer.findStudent(req.body, req.user.name, function(err, result){
    if(err){
      console.log(err)
      req.flash('error', err)
    }
    if(result.length){
      if(req.body.regid){
        // console.log(result)
        //res.send(result)
        res.redirect('/invoice/customer/' + result[0].id)
      }
      else{
        //res.send(result)
        res.locals.user = req.user
        res.render('invoice/list',{layout: 'org.hbs', title: 'Search Result', phead: 'Customer List', icon: 'list', data: result})
      }
    }
    else{
      req.flash('error', 'Found no Customers matching criteria')
      res.redirect('/invoice/customer')
    }
  })
})

router.post('/updoc', authsys.ensureAuthenticated, function(req, res){
  console.log('body parameters', req.body)
  let search = {}
  if(req.body.id) search._id = req.body.id
  if(req.body.name) search.name = req.body.name
  if(req.body.email) search.email = req.body.email
  if(req.body.phoneno) search.phoneno = req.body.phoneno

  Customer.find(search, function(err, result){
    if(err){
      console.log(err)
      req.flash('error', "ERROR IN DB")
      return res.redirect('/invoice/new')
    }
    if(result.length)
			if(req.body.id){
				req.session.client = result[0]
				//console.log('Session values: req.session[stud]: ', req.session[stud])
				res.locals.user = req.user
				res.render('invoice/updoc',{layout: 'org.hbs', title: 'Upload Document', phead: 'Upload Document To The Selected Customers', icon: 'file-o', data: result[0]})
			}
			else{
				//res.send(result)
				res.locals.user = req.user
				res.render('invoice/list',{layout: 'org.hbs', title: 'Search Result', phead: 'Customer List', icon: 'list', data: result})
			}
		else {
      req.flash('error', 'Found no Customers matching criteria')
      res.redirect('/invoice/new')
    }
    
  })
})

router.get('/docdetail', authsys.ensureAuthenticated, function(req, res){
  //console.log('Student id: ', req.params.id)
  //console.log('Session data: ', req.session)
  res.locals.user = req.user
  //console.log('Session values: ', req.session.client)
  if(req.session.client)
    Vendor.findOne({iaccount: req.user.id}, function(err, result){
      if(err) console.log(err)
      else
        res.render('invoice/docdetail',{layout: 'org.hbs', title: 'Select Document', phead: 'Select Document', icon: 'file-o', data: req.session.client, vendor: result})
    })
  else
    res.redirect('/invoice/new')
})

router.post('/docid.check', authsys.ensureAuthenticated, function(req, res){
  console.log("Body parameters: ", req.body)
  Doc.find({udocId: req.body.id}, function(err, result){
    if(err) res.send(false)
    if(result.length==0)
      res.send(true)
    else
      res.send(false)
  })
})

router.post('/pandp', upload.single('fsub'), authsys.ensureAuthenticated, function(req, res, next){
  // console.log('File Attributes', req.file)
  console.log('Body Attributes', req.body)
  if(!req.session.client){
    console.log('Session does not exist')
    res.redirect('/invoice/new')
  }
  else{
  // Document Session data
  console.log(' Session data: ', req.session.client)
	delete req.session.doc
  let doc = {};
  doc.receiverId = req.session.client._id
  doc.issuer     = req.user.name
  doc.issuerId   = req.user.id
  doc.receiver    = req.session.client.name
  doc.to				 = req.session.client.email 
  // Document info
  doc.udocId     = req.body.certId;
  doc.doi        = req.body.idate;
  doc.expiry     = false;
  if(req.body.expire == 'yes'){
    doc.expiry   = true
    doc.doe      = req.body.edate
  }
  doc.docType    = req.body.certType
  doc.pic        = req.session.client.logo
  doc.filename   = req.session.client._id + '_' +Date.now()
  doc.signatory  = req.body.signatory
  console.log('req.body.signatory: ', req.body.signatory)
  console.log('doc.signatory: ', doc.signatory)

  hashFiles({files: './public/uploads/'+req.file.filename, algorithm: 'sha256'}, function(error, hash){
    if (error)  console.log(error)
    else {
      //console.log(hash)
      doc.hash = hash;
      
      Doc.doesDocExist(hash, function(err, docResult){
        if(err) console.log(err)
        else{
          if(docResult && docResult.txid) {
            req.flash('error', 'The document already exist!')
            fs.unlink('./public/uploads/'+req.file.filename, (err) => {
              if (err) throw err;
              console.log(req.file.filename + ' was deleted');
            });
            res.redirect('/invoice/docdetail/')
          }
          else{
            console.log(doc)
            req.session.doc = doc
            var oldpath = './public/uploads/'+req.file.filename
            var newpath = './public/'+ req.user.id
            res.locals.user = req.user
            if(docResult){
              console.log("Docment exists")
            }
            else{
              console.log("Creating document")
              let receiver = {
                receiverId: doc.issuerId,
                name: doc.receiver,
                pic: doc.pic, 
              }
              let meta = {
                date: doc.doi,
                degType: doc.docType,
                signatory: doc.signatory
              }
              if(doc.doe)
                meta.expiry = doc.doe
              let newdoc = new Doc({
                hash: doc.hash,
                filename: doc.filename,
                issuerID: doc.issuerId,
                receiver: JSON.stringify(receiver),
                meta: JSON.stringify(meta),
              }).save(function(err, doc){
                if(err)
                  console.log(err)
                else{
                  console.log('Doc DB: ', doc)
                }
              })
            }
            res.render('invoice/pandp', {layout: 'org.hbs', title: 'Preview', phead: 'Preview and Sign', icon: 'file-o', data: req.session.client, doc: doc, user: req.user});  
            fs.stat('./public/'+ req.user.id, function(err, stat){
              if(err){
                if(err.code == 'ENOENT')
                  fs.mkdir(newpath, 0o777, function(err){
                    if(err) console.log(err)
                    else
                      fs.rename(oldpath, newpath +'/'+ doc.filename, (err) => {
                        if (err) throw err;
                        console.log('file move complete!');
                      });
                  })
              }
              else
                fs.rename(oldpath, newpath +'/'+ doc.filename, (err) => {
                  if (err) throw err;
                  console.log('file move complete!');
                });
            })
          }
        }
      })
    }
  })
  }
})

router.post('/confirm/', authsys.ensureAuthenticated, function(req, res){
	console.log(req.body)
  req.session.doc.hashSign = req.body.signature;
  let data = {};
  data = req.session.doc;
  console.log("data: ", data)
  //var sign = req.body.signature
  
  console.log("session: ", req.session.doc)
  
  data.hostname = req.hostname
  data.from = req.user.email
  //console.log("Hostname: ", req.hostname)
  req.session.doc.token = true
  res.location('/invoice/new')
  res.locals.user = req.user
  Setting.findOne({user: req.user.id}, function(err, setting){
    if(err){
      console.log(err)
      req.flash('error', 'Cannot access setting DB')
      return res.redirect('/invoice')
    }
    Doc.findOne({hash: data.hash}, function(err, doc){
      if(err){
        console.log(err)
        req.flash('error', 'Cannot access documents DB')
        return res.redirect('/invoice')
      }
      data.id = doc._id
      res.render('invoice/confirm', {layout: 'org.hbs', title: 'Confirm', icon: 'file-o', phead: 'Publish Document', data: data, setting: setting})
    })
  })
})

//Verifing Issuer Domain
router.get('/confirm.domain/', authsys.ensureAuthenticated, function(req, res){
	console.log("Request Domain: ", req.hostname)
	if(req.session.doc.token = true)
		res.json({"status": true, "msg": null})
	else
		res.json({"status": false, "msg": "Token doesn't exist"})
})

//Generating Document Hash
router.get('/confirm.hash/', authsys.ensureAuthenticated, function(req, res){
	if(req.session.doc.token = true){
		var filePath = './public/'+ req.session.doc.issuerId + '/' + req.session.doc.filename;
		hashFiles({files: filePath, algorithm: 'sha256'}, function(error, hash){
      if(error){
				console.log(error)
				res.json({"status": false, "msg": error})
			}
			else{
				if(hash == req.session.doc.hash)
					res.json({"status": true, "msg": null})
				else{
					req.session.doc.token = false
					res.json({"status": false, "msg": "Hash integrity failed"})
				}
			}
		})		
	}
	else
		res.json({"status": false, "msg": "Token doesn't exist"})
})

//Verifing Issuer Signature
router.get('/confirm.signature/', authsys.ensureAuthenticated, function(req, res){
	console.log("Pubkey: ", req.user.pubkey)
	console.log("Hash: ", req.session.doc.hash)
	console.log("Signature: ", req.session.doc.hashSign)
	if(req.session.doc.token = true) {
		verifySign(req.user.pubkey, req.session.doc.hash, req.session.doc.hashSign, function(isValid){
			console.log(isValid)
			if(isValid) {
				//req.session.doc.txId = SHA256('TXID'+req.session.doc.hashSign);
				res.json({"status": true, "msg": null})
			}
			else {
				req.session.doc.token = false
				res.json({"status": false, "msg": "Signature verification failed"})
			}
		})
	}
	else
		res.json({"status": false, "msg": "Token doesn't exist"})
})

//Uploading file to decentralised storage
router.get('/confirm.file/', authsys.ensureAuthenticated, function(req, res){
	console.log(req.session.doc.filename)
	if(req.session.doc.token = true){
		var filePath = './public/'+ req.session.doc.issuerId + '/' + req.session.doc.filename;
    var readStream = fs.createReadStream(filePath);

    var file = [{
      path: req.session.doc.filename,
      content: readStream
    }]

    Setting.findOne({user: req.user.id}, function(err, setting){
      if(err){
        console.log(err)
        return res.json({"status": false, "msg": err})
      }

      let ipfs = ipfsAPI('/ip4/' + setting.ipfs + '/tcp/5001')
      
      ipfs.files.add(file, function (err, files) {
        if (err){
          console.error(err);
          req.session.doc.token = false
          return res.json({"status": false, "msg": err})
        }
        else{
          console.log("IPFS fileid: ", files['0'].hash)
          req.session.doc.storjfileid = files['0'].hash
          Doc.findOneAndUpdate({hash: req.session.doc.hash}, {filename: req.session.doc.storjfileid}, function(err, doc){
            if (err){
              console.error(err);
              req.session.doc.token = false
              return res.json({"status": false, "msg": err})
            }
            // console.log(doc)
            res.json({"status": true, "msg": null})
          })
        }
      })
    })
	}
	else
		res.json({"status": false, "msg": "Token doesn't exist"})
})

router.get('/confirm.blockchain/', authsys.ensureAuthenticated, function(req, res){
	if(req.session.doc.token = true){
    console.log("Publishing to blockchain")
    Setting.findOne({user: req.user.id}, function(err, setting){
      if(err){
        console.log(err)
        return res.json({"status": false, "msg": err})
      }
      Doc.findOne({hash: req.session.doc.hash}, function(err, doc){
        if(err){
          console.log(err)
          return res.json({"status": false, "msg": err})
        }
        //console.log(doc)
        console.log("Connecting to Network API")
        request({
          url: setting.chain + '/channels/' + setting.channel + '/chaincodes/docchain',
          method: 'POST',
          headers: {
            Authorization: 'Bearer ' + setting.auth,
            'Content-Type': 'application/json'
          },
          json: {
            "fcn"  : "issueDocument",
            "args"    : [doc._id, doc.hash, req.session.doc.hashSign, doc.filename, doc.issuerID, doc.receiver, doc.meta]
          }
        }, function(rerr, rres, rbody) {
          if(rerr) {
            req.session.doc.token = false
            return res.json({"status": false, "msg": "Publishing to Blockchain failed"})
          }
          console.log(rbody)
          var txid = rbody
          var url = 'https://' + setting.domain + '/docs/' + doc._id
          if(txid.length == 64){
            res.json({"status": true, "msg": txid})
            QRCode.toDataURL(url, function (err, qr) {
              //console.log(qr)
              //res.json({'message':'Sucessfully Printed to Blockchain','txid': txid, 'url': url, 'qrcode':qr})
              var id = doc._id
              Doc.findByIdAndUpdate(id, {$set:{ txid: txid, qr: qr}}, { "new": true}, function(nerr, ndoc){
                if(nerr) console.log(nerr)
                else console.log(ndoc)
              })/*
              request({
                url: 'http://' + sitem.chain + ':8008/addTx?tx=' + txid,
                method: 'GET',
                headers: { Authorization: 'auditpass' }
              }, function(aerr, ares, abody){
                if(aerr)  console.log("Public audit: ", aerr)
                else  console.log("Public audit: ", ares.statusCode)
              })*/
            })
          }
          else{
            req.session.doc.token = false
            return res.json({"status": false, "msg": "Publishing to Blockchain failed"})
          }
        })
      })
    })
	}
	else
		res.json({"status": false, "msg": "Token doesn't exist"})
})

router.get('/settings', authsys.ensureAuthenticated, function(req, res){
  Vendor.findOne({iaccount: req.user.id}, function(err, result){
    if(err){
      console.log(err)
      res.redirect('/invoice')
    }
    res.locals.user = req.user
    Setting.findOne({user: req.user.id}, function(err, setting){
      if(err){
        console.log(err)
        res.redirect('/invoice')
      }
      res.render('invoice/set', {layout: 'org.hbs', title: 'Settings', data: result, setting: setting, phead: 'Account Setting', icon: 'cogs'}); 
    })
  })
})

router.post('/settings', authsys.ensureAuthenticated, function(req, res){
  console.log('Body parameters: ', req.body)
  var update = {}
  var msg;
  if(req.body.docType)
    {
      update.docType = req.body.docType
      msg = req.body.docType+' has been added to "Type of Document"'
    }
  if(req.body.signatory) 
  {
    update.signatory = req.body.signatory
    msg = req.body.signatory+' has been added to "Authorized Signatory"'  
  }
  if(req.body.docType && req.body.signatory)
    msg = '"'+req.body.signatory+'" & "'+req.body.docType+'" has been added to "Authorized Signatory" and "Type of Document" respectively'  
  req.flash('success', msg)
  Vendor.findOneAndUpdate({iaccount: req.user.id}, {$addToSet: update}, { "new": true, "upsert": true }, function(err, result){
    if(err) console.log(err)
    else{
      res.redirect('/invoice/settings') 
    }
  })
})

router.post('/settings.signatory', authsys.ensureAuthenticated, function(req, res){
  console.log('Body parameters: ', req.body)
  var update = {}
  update.signDetail = req.body
  console.log('Update value', update)
  Vendor.findOneAndUpdate({iaccount: req.user.id}, {$pull: { 'signDetail': {  docType: req.body.docType }}}, function(err, result){
    if(err) console.log(err)
    else{
      //console.log("After Entry Removal", result)
      Vendor.findOneAndUpdate({iaccount: req.user.id}, {$addToSet: update}, { "new": true, "upsert": true }, function(err, result){
        if(err) console.log(err)
        else{
          //console.log('after signdetails update', result)
          req.flash('success', '"'+req.body.docType+'" is linked with "'+req.body.signatory+'"')
          res.redirect('/invoice/settings')  
        }
      })
    }
  })
})

router.get('/settings.remove.signatory', authsys.ensureAuthenticated, function(req, res){
  console.log('Doctype-Signatory to be removed : ', req.query.docType)
  Vendor.findOneAndUpdate({iaccount: req.user.id}, {$pull: { 'signDetail': {  docType: req.query.docType }}}, function(err, result){
    if(err) console.log(err)
    else{
      req.flash('error', req.query.docType+" has been removed from the list of Authorised Signatories")
			res.redirect('/invoice/settings')
    }
  })
})

router.post('/revoke', authsys.ensureAuthenticated, function(req, res){
  console.log(req.body)
  verifySign(req.user.pubkey, req.body.docid, req.body.signature, isvalid => {
    console.log('verifying signature: '+isvalid)
    if(isvalid){
      Setting.findOne({user: req.user._id}, function(serr, sitem) {
        if(serr)
          return res.json({'message':'ERROR:: Cannot find settings for user', 'error': serr, 'action': 'Contact support'})
        else{
          console.log("sending request")
          request({
            url: sitem.chain + '/channels/' + sitem.channel + '/chaincodes/docchain',
            method: 'POST',
            headers: {
              Authorization: 'Bearer ' + sitem.auth,
              'Content-Type': 'application/json'
            },
            json: {
              "fcn"  : "revokeDocument",
              "args"    : [req.body.docid, req.body.signature]
            }
          }, function(rerr, rres, rbody){
            if(rerr) {
              req.flash('error', 'Revocation failed')
              return res.redirect('/invoice/new')
            }
            console.log(rres.statusCode)
            if(rres.statusCode==200){
              var txid = rbody.txid
              Doc.findByIdAndUpdate(req.body.id, {$set:{ txid: txid, revoked: true}}, { "new": true}, function(nerr, ndoc){
                if(nerr) {
                  console.log(nerr)
                  req.flash('error', 'Revocation sucsess, DB update failed')
                  return res.redirect('/invoice/new')
                }
                console.log(ndoc)
                req.flash('success', 'Successfully revoked the document')
                return res.redirect('/invoice/new')
              })
            }
            else{
              req.flash('error', 'Revocation failed')
              res.redirect('/invoice/new')
            }
          })
        }
      })
    }
    else{
      req.flash('error', 'Signature verification failed')
      res.redirect('/invoice/new')
    }
  })
})

module.exports = router;

/********** CUSTOM FUNCTIONS ***********/
const verifySign = function(pubkey, msg, signature, callback){
  let isValid;
  let sig = new jsrsaSign.crypto.Signature({"alg": 'SHA256withECDSA', "prov": "cryptojs/jsrsa"});
  sig.init({xy: pubkey, curve: 'secp256r1'});
  sig.updateString(msg);
  isValid = sig.verify(signature);
  callback(isValid);
}
