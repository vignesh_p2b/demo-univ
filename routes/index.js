const express = require('express');
const router = express.Router();

/********* Merkel Tree  ******/
const MerkleTools = require('merkle-tools')
const merkleTools = new MerkleTools

var dataSum = '';
var dataCount = 1;

/********* EThereum PA ******/
var Web3 = require('web3');
require('dotenv').config()

var web3 = new Web3(
    new Web3.providers.HttpProvider('https://rinkeby.infura.io/v3/5e7d9c2418ac4065afb780684e60c205')
);

var key = process.env.ETH_PASS
const account = web3.eth.accounts.privateKeyToAccount('0x' + key);
console.log('Address: ', account)

web3.eth.accounts.wallet.add(account);
web3.eth.defaultAccount = account.address;

var interface = [
	{
		"constant": true,
		"inputs": [
			{
				"name": "id",
				"type": "uint256"
			}
		],
		"name": "getLeaf",
		"outputs": [
			{
				"name": "",
				"type": "uint256"
			},
			{
				"name": "",
				"type": "string"
			},
			{
				"name": "",
				"type": "string"
			},
			{
				"name": "",
				"type": "uint256"
			}
		],
		"payable": false,
		"stateMutability": "view",
		"type": "function"
	},
	{
		"constant": true,
		"inputs": [],
		"name": "info",
		"outputs": [
			{
				"name": "",
				"type": "string"
			},
			{
				"name": "",
				"type": "uint256"
			}
		],
		"payable": false,
		"stateMutability": "view",
		"type": "function"
	},
	{
		"constant": false,
		"inputs": [],
		"name": "kill",
		"outputs": [],
		"payable": false,
		"stateMutability": "nonpayable",
		"type": "function"
	},
	{
		"constant": false,
		"inputs": [
			{
				"name": "data",
				"type": "string"
			},
			{
				"name": "hash",
				"type": "string"
			},
			{
				"name": "root",
				"type": "string"
			}
		],
		"name": "addLeaf",
		"outputs": [],
		"payable": false,
		"stateMutability": "nonpayable",
		"type": "function"
	},
	{
		"constant": false,
		"inputs": [
			{
				"name": "newOwner",
				"type": "address"
			}
		],
		"name": "changeOwner",
		"outputs": [],
		"payable": false,
		"stateMutability": "nonpayable",
		"type": "function"
	},
	{
		"constant": true,
		"inputs": [],
		"name": "Owner",
		"outputs": [
			{
				"name": "",
				"type": "address"
			}
		],
		"payable": false,
		"stateMutability": "view",
		"type": "function"
	},
	{
		"inputs": [],
		"payable": false,
		"stateMutability": "nonpayable",
		"type": "constructor"
	}
];

var contractAddress = '0x00e7D08d1A325D4205E44A2DF86dEd4c6280DAf2';
var contract = new web3.eth.Contract(interface, contractAddress);

/********** Custom dependencies ****/
var authsys = require('../cjs/authsys')
var token = require('../models/token')
var issuer = require('../models/issuer')

/********* Node Mailer **************/

var nodemailer = require('nodemailer');

var transporter = nodemailer.createTransport({
  service: 'gmail',
  auth: {
    user: 'mail.vignesh1793@gmail.com',
    pass: '123456789'
  }
});

/********* Routes **********/
router.get('/', function(req, res){
  res.redirect('/login')
})

router.post('/', authsys.authenticate('local', {failureRedirect: '/login', failureFlash: 'Invalid username or password'}),
  function(req, res){
    //console.log(req.body);
    //console.log(req.user);
    req.flash('success', 'You are now logged in');
    //console.log(res.locals)
    if(req.user.accType == 'admin') res.redirect('/admin')
    else    res.redirect('/user')
  }
)
router.get('/login', authsys.isLoggedIn, function(req, res){
  res.render('main/login', {layout: 'login.hbs', title: 'Login'})
})

router.get('/logout', function(req, res){
  req.logout();
  req.flash('success', 'You are now logged out')
  res.location('/')
  res.redirect('/login')
})
/*
router.get('/profile', function(req, res){
  res.render('user/profile', {layout: 'homeLayout.hbs', title: 'Profile'})
})

router.post('/profile', function(req, res){
  console.log(req.body)
  //res.render('user/profile', {layout: 'homeLayout.hbs', title: 'Profile'})
})
*/

router.post('/forgotpwd', function(req, res){
  console.log("Body parameters: ", req.body)
  token.findOne({email: req.body.email}, function(err, result){
    if(err){
      console.log('Token find error: ', err)
      req.flash('error','Token find error')
      res.redirect('/login')
    }
    console.log(result)
    if(result != null){
      req.flash('error','A token is already generated for this email account')
      res.redirect('/login')
    }
    else{
      token.issueToken(req.body.email, function(err, result){
        if(err) {
          console.log('Error creating token')
          if(err == 'noUser')
            req.flash('error', 'Email dosen\'t exist or is not registered')
          else
            req.flash('error', 'Error creating token')
          res.redirect('/login')
        }
        else{
          req.flash('success', 'A password reset mail has been sent to '+req.body.email+'. For Testing the "RESET TOKEN" is '+result)
          res.redirect('/changepwd?email='+req.body.email)
        }
      })
    }
  })
})

router.get('/changepwd', function(req, res){
  if(!req.query.email) {
    console.log('url incomplete')
    req.flash('error', 'Incomplete password reset URL')
    res.redirect('/login')
  }
  else{
    token.findOne({email: req.query.email}, function(err, result){
      if(err){
        console.log('Token find error: ', err)
        req.flash('error','Token find error')
        res.redirect('/login')
      }
      console.log(result)
      if(result != null){
        res.render('main/changepwd', {layout: 'homeLayout.hbs', title: 'Profile', email: req.query.email})
      }
      else{
        req.flash('error','There is no "Token key" generated for this account')
        res.redirect('/login')
      }
    })
  }
})

router.post('/changepwd', function(req, res){
  console.log('Body parameters: ', req.body)
  issuer.findOneAndUpdate()
  token.findOne({email : req.body.email}, function(err, result){
    if(err){
      console.log('Token find error: ', err)
      req.flash('error','Token find error')
      res.redirect('/login')
    }
    else{
      if(result.token == req.body.token){
        token.findOneAndRemove({email : req.body.email}, function(rerr, rresult){
          if(rerr) console.log('Token remove error: ', rerr)
          else console.log('Token remove result: ', rresult)
        })
        issuer.changepwd(req.body.email, req.body, function(err, msg){
          if(err) console.log(err)          
          if(msg=='ok')
            req.flash('success','Password has been successfully changed')
          res.redirect('/login')
        })
      }
      else{
        req.flash('error','Incorrect TOKEN key entered')
        res.redirect('/changepwd?email='+req.body.email)
      }
    }
  })
})

/*********** Mail Routes **********/

router.post('/mail', authsys.ensureAuthenticated, function(req, res){
  //console.log('Mail data from confirm page.')
  //console.log(req.body)

  var mailOptions = {
    from: req.body.from,
    to: req.body.to,
    subject: req.body.subject,
    html: req.body.content
  };
  
  transporter.sendMail(mailOptions, function(error, info){
    if (error) {
      console.log(error);
      res.writeHead(204)
      res.send('Mail failed to send.<br>'+error)
    } else {
      console.log('Email sent: ' + info.response);
      res.send('Mail sent succesfully')
    }
  });
})

/************* Merkel Routes ************/

router.post('/addLeaf', authsys.ensureAuthenticated, function(req, res){
  console.log('Merkel data from confirm page.')
  console.log(req.body)

  if(true){
    dataSum = req.body.data;
  }

  if(true){
    merkleTools.addLeaf(dataSum, true)
    var leafCount = merkleTools.getLeafCount()
    var leafValue =  merkleTools.getLeaf(leafCount-1)
    leafValue = leafValue.toString('hex')
    

    merkleTools.makeTree(false)
    var rootValue = merkleTools.getMerkleRoot()
    rootValue = rootValue.toString('hex')

    console.log('Data: '+dataSum+' Hash: '+leafValue+' Root: ',rootValue)
  
    contract.methods.addLeaf(req.body.data, leafValue, rootValue)
      .send({
      from: web3.eth.defaultAccount,
      gas: 2000000
      }, function(err, result){
      if(err){
        console.log(err);
      } else {
        console.log("Transaction Id: ", result);
        res.json({paid: result, id: leafCount-1});
        dataSum = '';
      }
    })
  }
})

router.get('/PAinfo', function(req, res){
  contract.methods.info().call({from: web3.eth.defaultAccount}, function(err, result){
    if(err){
       console.log(err);
    } else {
      console.log("***** Current Public Audit state *****")
      console.log('Merkel Root: '+result['0']+'\nLeafs Count: '+result['1']);
      res.send('{root: '+result['0']+', count: '+result['1']+'}')
    }
  })
})

router.post('/proof', function(req, res){
  console.log(req.body)
  var id = Number(req.body.id)
  var leafValue =  merkleTools.getLeaf(id)
  console.log("Leaf Value: ", leafValue.toString('hex'))

  var proof = merkleTools.getProof(id)
  console.log("Proof: ", proof)

  console.log("Pulling Merkel-Root from Public Blockchain('Ethereum')")
  contract.methods.info().call({from: web3.eth.defaultAccount}, function(err, result){
    if(err){
       console.log(err);
    } else {
      console.log("***** Current Public Audit state *****")
      console.log('Merkel Root: '+result['0']+'\nLeafs Count: '+result['1']);
      var isValid = merkleTools.validateProof(proof, leafValue, result['0'])
      console.log("Is Valid: ", isValid)
      res.json({status: 'SUCCESS', value: leafValue.toString('hex'), root: result['0'], path: JSON.stringify(proof)})
    }
  })
})

router.get('/buildTree', function(req, res){
  var dataArray = [];
  merkleTools.resetTree()
  contract.methods.info().call({from: web3.eth.defaultAccount}, function(err, result){
    if(err){
       console.log(err);
    } else {
      console.log(result);
      var count = result['1']
      var root = result['0']

      console.log('Starting Sync: '+Date.now());

      for(i=0; i<count; i++){
        //console.log(i+' '+count)
        if(i == (count-1))
          console.log('Sync Complete: '+Date.now());
        //console.log(Date.now());
        contract.methods.getLeaf(i).call().then(function(leaf){
          //console.log('leaf data: ', leaf)
          dataArray.push(leaf)
          //console.log(i+'  '+Date.now());
          //console.log(dataArray)
        }).catch(function(err){
          console.log(err)
        })
      }

      setTimeout(function(){
        //console.log(dataArray)  // Befor Sort
        dataArray.sort(function(a,b){return a[0]-b[0]})
        console.log(dataArray)  // After Sort

        dataArray.forEach(element => {
          //console.log(element[2])
          merkleTools.addLeaf(element[2], false)
        });

        console.log("Total Leafs Synced: ", merkleTools.getLeafCount())
        
        merkleTools.makeTree(false)
        var rootValue = merkleTools.getMerkleRoot()
        console.log("Merkel Root = ", rootValue.toString('hex'))
        if(rootValue.toString('hex')==root){
          console.log('Tree built successfully')
          res.send('SUCCESS')
        } else {
          console.log('Tree build failed')
          res.send('FAILED')
        }
      }, 1000+40*count)
    }
  })
})

module.exports = router;
