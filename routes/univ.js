const express = require('express');
const router = express.Router();

var Issuer = require('../models/issuer')
var Univ = require('../models/univ')
var Student = require('../models/student')
var authsys = require('../cjs/authsys')
var Doc = require('../models/doc')
const multer  = require('multer');
var upload = multer({ dest: 'public/uploads/' })

const fs  = require('fs');
const hashFiles = require('hash-files');
const request = require('request');
const SHA256 = require("crypto-js/sha256");
const jsrsaSign = require('jsrsasign');
const ipfsAPI = require('ipfs-api')
var ipfs = ipfsAPI('/ip4/127.0.0.1/tcp/5001')

/********* Routes **********/
router.get('/new', authsys.ensureAuthenticated, function(req, res){
  res.locals.user = req.user
  Univ.findOne({dcid: req.user.id}, function(err, result){
    console.log(result)
    req.session['univ'] = result
    //console.log('req.session.univ: ', req.session['univ'])
    if(req.user.pubkey == undefined || req.user.pubkey == null){
      req.flash('error', 'This account does not have a public key. Only accounts with public key can publish documents. Please contact support@docchain.io')
      res.redirect('/user')
    }
    else{
      res.render('univ/new',{layout: 'univ.hbs', title: 'Search Student', data: result, phead: 'Search Student', icon: 'file-o'})
    }
  })
})

router.get('/course', authsys.ensureAuthenticated, function(req, res){
  res.locals.user = req.user
  Univ.findOne({dcid: req.user.id}, function(err, result){
    //console.log(result.id + ' profile data: ', result)
    res.render('univ/course',{layout: 'univ.hbs', title: 'Course', data: result, phead: 'Courses and Subjects', icon: 'plus-circle'})
  })
})

router.post('/course/add', authsys.ensureAuthenticated, function(req, res){
  console.log(req.body)
  Univ.updateCourse(req.user.id, req.body, function(err){
    if(err){
      console.log(err)
      req.flash('error',err)
      res.redirect('/univ/course')
    }
    else{
      //console.log('Updated Univ profile: ', result)
      req.flash('success','Course details updated')
      res.redirect('/univ/course')
    }
  })
})

/*
router.get('/student/', authsys.ensureAuthenticated, function(req, res){
  Univ.findOne({dcid: req.user.id}, function(err, result){
    res.render('univ/student',{layout: 'univ.hbs', title: 'Student', phead: 'Student', data: result})
  })
})
*/

router.get('/student', authsys.ensureAuthenticated, function(req, res){
  console.log(req.query.id)
  console.log('enroll: ', req.query.enroll)
  console.log('show: ', req.query.show)
  Student.findById(req.query.id, function(err, result){
    if(err){
      console.log(err)
      req.flash('error', err)
      res.redirect('/user')
    }
    else{
      //console.log(result)
      if(result){
        res.locals.user = req.user
        if (req.query.enroll == 'true')
          enroll = true
        else
          enroll = false
        if (req.query.show == 'true')
          show = true
        else
          show = false
        res.render('univ/profile',{layout: 'univ.hbs', title: 'Student Profile', data: result, phead: 'Student Profile', icon: 'user-circle-o', enroll: enroll, show: show})
      }
      else{
        req.flash('error', 'The Student does not exist')
        res.redirect('/univ/student/enroll')
      }
    }
  })
})

router.get('/student/add', authsys.ensureAuthenticated, function(req, res){
  res.locals.user = req.user;
  res.render('univ/add',{layout: 'univ.hbs', title: 'Add Student', phead: 'Add Student', icon: 'user-plus'})
})

router.post('/student/add', upload.single('fsub'), authsys.ensureAuthenticated, function(req, res){
  //console.log('Given value', req.body)
  //console.log('File Attributes', req.file)
  var filename = req.body.sfname +'_'+ req.body.slname +'_'+ Date.now();
  
  var data = {};
  data.student = {};
  data.father = {};
  if(req.body.sfname) data.student.fname = req.body.sfname
  if(req.body.smname) data.student.mname = req.body.smname
  if(req.body.slname) data.student.lname = req.body.slname
  if(req.body.ffname) data.father.fname = req.body.ffname
  if(req.body.fmname) data.father.mname = req.body.fmname
  if(req.body.flname) data.father.lname = req.body.flname
  if(req.body.dob) data.dob = req.body.dob
  if(req.body.email) data.email = req.body.email
  if(req.body.phoneno) data.phoneno = req.body.phoneno
  //console.log(data)

  Student.doesStudentExist(data, function(err, exist){
    if(err) console.log(err)
    else if (exist){
      req.flash('error', 'The student already exists!')
      res.redirect('/univ/student?id='+exist+'&enroll=false&show=false')
      fs.unlink('./public/uploads/'+req.file.filename, (err) => {
        if (err) throw err;
        console.log(req.file.filename + ' was deleted');
      });
    }
    else{
      fs.rename('./public/uploads/'+req.file.filename, './public/students/'+ filename, (err) => {
        if (err) throw err;
        console.log('Rename complete!');
      }); 
      
      // updataing data object
      data.pic = filename
      // finalising data object for student creation
      data = new Student(data)
      // creating student object in DB
      Student.createStudent(data, function(err, student){
        if(err) {
          console.log(err)
          req.flash('error', err)
					res.redirect('/univ/student/add')
        }
        else{
          console.log(student)
          req.flash('success', 'A student account has been successfully created, Fillout the course details below to Enroll the newly created student.')
          req.session.add = true;
					res.redirect('/univ/student/enroll/'+student.id)
        }
      })
    }
  })
})

router.get('/student/enroll', authsys.ensureAuthenticated, function(req, res){
  res.locals.user = req.user;
  res.render('univ/enroll_id',{layout: 'univ.hbs', title: 'Enroll Student', phead: 'Enroll Student', icon: 'user-plus'})
})

router.get('/student/enroll/:id', authsys.ensureAuthenticated, function(req, res){
  console.log(req.params.id)
  res.locals.user = req.user
  Univ.findOne({dcid: req.user.id}, function(err, result){
    result.studid = req.params.id
    //console.log("Result after modify: ", result)
    res.render('univ/enroll',{layout: 'univ.hbs', title: 'Enroll Student', phead: 'Enroll Student', icon: 'user-plus', data: result})
  })
})

router.post('/student/enroll/:id', authsys.ensureAuthenticated, function(req, res){
  console.log("Body Parametrs: ", req.body)
  req.body.id = req.params.id;
  req.body.univ = req.user.name;
  req.session.course = req.body;
  res.locals.user = req.user;
  console.log("session course: ", req.session.course)
  Student.findById(req.params.id, function(err, result){
    res.render('univ/sprev',{layout: 'univ.hbs', title: 'Confirm Student Enrollment', phead: 'Confirm for Enrollment', icon: 'user-plus', data: result, course: req.body})
  })  
})

router.get('/student/enroll.course', authsys.ensureAuthenticated, function(req, res){
  console.log("session course: ", req.session.course)
  Student.joinCourse(req.session.course, req.user.name, function(err, result){
    delete req.session.course
    if(err && err != 'exist'){
      console.log(err)
      req.flash('error', err)
      if(req.session.add){
        req.session.add = false
        res.redirect('/univ/student/add')
      }
      else
        res.redirect('/univ/student/enroll')
    }
    else if(err == 'exist'){
      req.flash('error', 'The Student is already enrolled for this course')
      res.redirect('/univ/student/enroll')
    }
    else{
      req.flash('success', 'The Student is successfully enrolled')
      //res.send(result)
      if(req.session.add){
        req.session.add = false
        res.redirect('/univ/student/add')
      }
      else
        res.redirect('/univ/student/enroll')
    }
  })
})

router.get('/session.clear/course', authsys.ensureAuthenticated, function(req, res){
  delete req.session.course
  //console.log("session after course delete: ", req.session)
  res.redirect('/univ/new')
})

router.post('/student/list', authsys.ensureAuthenticated, function(req, res){
  // console.log(req.body)
  Student.findStudent(req.body, req.user.name, function(err, result){
    if(err){
      console.log(err)
      req.flash('error', err)
    }
    if(result.length){
      if(req.body.regid){
        // console.log(result)
        //res.send(result)
        res.redirect('/univ/student/' + result[0].id)
      }
      else{
        //res.send(result)
        res.locals.user = req.user
        res.render('univ/list',{layout: 'univ.hbs', title: 'Search Result', phead: 'Student List', icon: 'list', data: result})
      }
    }
    else{
      req.flash('error', 'Found no students matching criteria')
      res.redirect('/univ/student')
    }
  })
})

router.post('/updoc', authsys.ensureAuthenticated, function(req, res){
  console.log('body parameters', req.body)
  Student.findStudent(req.body, req.user.name, function(err, result){
    if(err){
      console.log(err)
      req.flash('error', err)
    }
    if(result.length){
      if(req.body.regid){
        // console.log(result)
        //res.send(result)
        var stud = result[0].course[0].regid
        //console.log('session id: ', stud)
        req.session[stud] = result[0]
        //console.log('Session values: req.session[stud]: ', req.session[stud])
        res.locals.user = req.user
        res.render('univ/updoc',{layout: 'univ.hbs', title: 'Upload Document', phead: 'Upload Document To The Selected Student', icon: 'file-o', data: result[0]})
      }
      else{
        //res.send(result)
        res.locals.user = req.user
        res.render('univ/list',{layout: 'univ.hbs', title: 'Search Result', phead: 'Student List', icon: 'list', data: result})
      }
    }
    else{
      req.flash('error', 'Found no students matching criteria')
      res.redirect('/univ/new')
    }
  })
})

router.get('/docdetail/:id', authsys.ensureAuthenticated, function(req, res){
  //console.log('Student id: ', req.params.id)
  //console.log('Session data: ', req.session)
  res.locals.user = req.user
  if(req.session[req.params.id])
    Univ.findOne({dcid: req.user.id}, function(err, result){
      if(err) console.log(err)
      else
        res.render('univ/docdetail',{layout: 'univ.hbs', title: 'Search Result', phead: 'Student List', icon: 'file-o', data: req.session[req.params.id], univ: result})
    })
  else
    res.redirect('/univ/new')
})

router.post('/docid.check', authsys.ensureAuthenticated, function(req, res){
  console.log("Body parameters: ", req.body)
  Doc.find({udocId: req.body.id}, function(err, result){
    if(err) res.send(false)
    if(result.length==0)
      res.send(true)
    else
      res.send(false)
  })
})

router.post('/pandp/:id', upload.single('fsub'), authsys.ensureAuthenticated, function(req, res, next){
  // console.log('File Attributes', req.file)
  console.log('Body Attributes', req.body)
  if(!req.session[req.params.id]){
    console.log('Session does not exist')
    res.redirect('/univ/new')
  }
  else{
  // Document Session data
  console.log(req.params.id+' Session data: ', req.session[req.params.id])

  var doc = {};
  doc.receiverId = req.session[req.params.id]._id
  doc.issuer     = req.user.name
  doc.issuerId   = req.user.id
  doc.student    = ''
  if(req.session[req.params.id].student.fname)
		doc.student = doc.student + req.session[req.params.id].student.fname + ' ' 
	if(req.session[req.params.id].student.mname)
		doc.student = doc.student + req.session[req.params.id].student.mname + ' ' 
	if(req.session[req.params.id].student.lname)
		doc.student = doc.student + req.session[req.params.id].student.lname;
  //doc.dob        = req.session[req.params.id].dob;
  //doc.pic        = req.session[req.params.id].pic;
  // Student course info
  doc.to				 = req.session[req.params.id].email 
  doc.regid      = req.session[req.params.id].course[0].regid;
  doc.degType    = req.session[req.params.id].course[0].degType;
  doc.degree     = req.session[req.params.id].course[0].degree;
  doc.course     = req.session[req.params.id].course[0].course;
  doc.year       = req.session[req.params.id].course[0].year;
  // Document info
  doc.udocId     = req.body.certId;
  doc.doi        = req.body.idate;
  doc.docType    = req.body.certType;
  doc.filename   = req.session[req.params.id]._id + '_' +Date.now()
  // Univ info
  doc.signatory  = req.body.signatory
  console.log('req.body.signatory: ', req.body.signatory)
  console.log('doc.signatory: ', doc.signatory)

  hashFiles({files: './public/uploads/'+req.file.filename, algorithm: 'sha256'}, function(error, hash){
    if (error)  console.log(error)
    else {
      //console.log(hash)
      doc.hash = hash;
      
      Doc.doesDocExist(hash, function(err, docResult){
        if(err) console.log(err)
        else{
          if(docResult){
            req.flash('error', 'The document already exist!')
            fs.unlink('./public/uploads/'+req.file.filename, (err) => {
              if (err) throw err;
              console.log(req.file.filename + ' was deleted');
            });
            res.redirect('/univ/docdetail/' + req.params.id)
          }
          else{
            console.log(doc)
            req.session.doc = doc
            var oldpath = './public/uploads/'+req.file.filename
            var newpath = './public/'+ req.user.id
            res.locals.user = req.user
            res.render('univ/pandp', {layout: 'univ.hbs', title: 'Preview', phead: 'Preview and Sign', icon: 'file-o', data: req.session[req.params.id], doc: doc, user: req.user});  
            fs.stat('./public/'+ req.user.id, function(err, stat){
              if(err){
                if(err.code == 'ENOENT')
                  fs.mkdir(newpath, 0o777, function(err){
                    if(err) console.log(err)
                    else
                      fs.rename(oldpath, newpath +'/'+ doc.filename, (err) => {
                        if (err) throw err;
                        console.log('file move complete!');
                      });
                  })
              }
              else
                fs.rename(oldpath, newpath +'/'+ doc.filename, (err) => {
                  if (err) throw err;
                  console.log('file move complete!');
                });
            })
          }
        }
      })
    }
  })
  }
})

router.post('/confirm/', authsys.ensureAuthenticated, function(req, res){
	console.log(req.body)
  req.session.doc.hashSign = req.body.signature;
  var data = {};
  data = req.session.doc;
  //var sign = req.body.signature
	data.txId = SHA256('TXID'+req.body.signature).toString();
  data.p2bId = SHA256(req.body.signature).toString();
  
  //console.log("txid: ", data.txId)
  //console.log("p2bId: ", data.p2bId)
  
  req.session.doc.txId = data.txId
  req.session.doc.p2bId = data.p2bId
  
  console.log("session: ", req.session.doc)
  
  data.hostname = req.hostname
  data.from = req.user.email 
  console.log("Hostname: ", req.hostname)
  Issuer.removeOneSlot(req.user.email, function(uerr, hasSlot){
    if(uerr && uerr != 'noSlot'){
      console.log(uerr)
    }
    if(uerr == 'noSlot'){
      req.flash('error', "You have 0 slot. Please purchase more to upload documents.")
			res.redirect('/univ/new')
    }
    if(hasSlot){
      req.session.doc.token = true
      res.location('/univ/new')
      res.locals.user = req.user
			res.render('univ/confirm', {layout: 'univ.hbs', title: 'Confirm', icon: 'file-o', phead: 'Publish Document', data: data})
		}
	})
})

//Verifing Issuer Domain
router.get('/confirm.domain/', authsys.ensureAuthenticated, function(req, res){
	console.log("Request Domain: ", req.hostname)
	if(req.session.doc.token = true)
		res.json({"status": true, "msg": null})
	else
		res.json({"status": false, "msg": "Token doesn't exist"})
})

//Generating Document Hash
router.get('/confirm.hash/', authsys.ensureAuthenticated, function(req, res){
	if(req.session.doc.token = true){
		var filePath = './public/'+ req.session.doc.issuerId + '/' + req.session.doc.filename;
		hashFiles({files: filePath, algorithm: 'sha256'}, function(error, hash){
      if(error){
				console.log(error)
				res.json({"status": false, "msg": error})
			}
			else{
				if(hash == req.session.doc.hash)
					res.json({"status": true, "msg": null})
				else{
					req.session.doc.token = false
					res.json({"status": false, "msg": "Hash integrity failed"})
				}
			}
		})		
	}
	else
		res.json({"status": false, "msg": "Token doesn't exist"})
})

//Verifing Issuer Signature
router.get('/confirm.signature/', authsys.ensureAuthenticated, function(req, res){
	console.log("Pubkey: ", req.user.pubkey)
	console.log("Hash: ", req.session.doc.hash)
	console.log("Signature: ", req.session.doc.hashSign)
	console.log("isRSA: ", req.user.isRSA)
	if(req.session.doc.token = true){
		verifySign(req.user.pubkey, req.session.doc.hash, req.session.doc.hashSign, req.user.isRSA, function(isValid){
			console.log(isValid)
			if(isValid){
				//req.session.doc.txId = SHA256('TXID'+req.session.doc.hashSign);
				res.json({"status": true, "msg": null})
			}
			else{
				req.session.doc.token = false
				res.json({"status": false, "msg": "Signature verification failed"})
			}
		})
	}
	else
		res.json({"status": false, "msg": "Token doesn't exist"})
})

//Uploading file to decentralised storage
router.get('/confirm.file/', authsys.ensureAuthenticated, function(req, res){
	console.log(req.session.doc.filename)
	if(req.session.doc.token = true){
		var filePath = './public/'+ req.session.doc.issuerId + '/' + req.session.doc.filename;/*

    })*/
    var readStream = fs.createReadStream(filePath);

    var file = [{
      path: req.session.doc.filename,
      content: readStream
    }]

    ipfs.files.add(file, function (err, files) {
      if (err){
        console.error(err);
        req.session.doc.token = false
        res.json({"status": false, "msg": "IPFS is encountering connection issues."})
      }
      else{
        console.log("IPFS fileid: ", files['0'].hash)
        req.session.doc.storjfileid = files['0'].hash
        res.json({"status": true, "msg": null})
      }
    })    
	}
	else
		res.json({"status": false, "msg": "Token doesn't exist"})
})

//Publishing On Blockchain
router.get('/confirm.blockchain/', authsys.ensureAuthenticated, function(req, res){
	console.log(req.session.doc)
	if(req.session.doc.token = true){
		request({
			url: 'http://34.93.244.135:8000/invoke',
			method: 'POST',
			json: {
        "channel"   : "mychannel",
        "chaincode" : "docchain",
        "function"  : "issueDocument",
        "args"      : [req.session.doc.p2bId, req.session.doc.hash, req.session.doc.hashSign, req.session.doc.storjfileid, req.session.doc.issuerId, req.session.doc.receiverId, "metadata"]
			}
		}, function(rerr, rres, rbody){
			//console.log(rres)
			console.log(rbody)
			if(rbody.status == 'OK')
				res.json({"status": true, "msg": null})
			else{
				req.session.doc.token = false
				res.json({"status": false, "msg": "Publishing to Blockchain failed"})
			}
		})
	}
	else
		res.json({"status": false, "msg": "Token doesn't exist"})
})
//Generating True Copy Link
router.get('/confirm.link/', authsys.ensureAuthenticated, function(req, res){
	//req.session.doc.p2bId = SHA256(req.session.doc.hashSign)

	if(req.session.doc.token = true){
		Doc.findOneAndUpdate({p2bId: req.session.doc.p2bId}, req.session.doc, { "new": true, "upsert": true }, function(err, doc){
			if(err)	{
				console.log(err)
				res.json({"status": false, "msg": err})
			}
			else{
				req.session.doc.token = false
				res.json({"status": true, "msg": null})
			}
		})
	}
	else
		res.json({"status": false, "msg": "Token doesn't exist"})
})

router.post('/update.certificate/', authsys.ensureAuthenticated, function(req, res){
  console.log('Data: ', req.body)
  Doc.findOneAndUpdate({p2bId: req.body.p2bid},{$set:{PAtxId: req.body.paid, PAleafId: req.body.id}}, {new: true}, function(err, result){
    if(err){
      console.log(err)
      res.send('FAILED')
    } else{
      console.log(result)
      if(result.PAtxId == req.body.paid){
        res.send('SUCCESS')
      }
    }
  })
})

router.get('/certificate/:id', function(req, res){
  console.log(req.params.id);
  Doc.findOne({p2bId: req.params.id}, function(err, result){
    res.render('univ/certificate', {data: result, title: 'Certificate', verify: false, layout: 'newlayout.hbs'})
  })
})

router.post('/certificate/:id', function (req, res) {
  var stat=0;
  // Fetching encrypted digest from blockchain
  Doc.findOne({p2bId: req.params.id}, function(err, result){
    console.log(result)
    request({
			url: 'http://34.93.244.135:8000/query',
			method: 'POST',
			json: {
        "channel"   : "mychannel",
        "chaincode" : "docchain",
        "function"  : "queryDocument",
        "args"      : [result.p2bId]
			}
		}, function(reqErr, reqRes, reqBody){
			if (reqErr) console.log(reqErr);
			else{
        console.log('Encrypted hash obtained: ', reqBody);
				stat+=1;
        //reqBody = JSON.parse(reqBody);
        //console.log(reqBody)
        reqBody =  JSON.parse(reqBody.response)
				encHash=reqBody.signature;
				console.log('Encrypted hash: ', encHash);
				//certId=reqBody.certId;
				//console.log('Certificate ID: ', certId);
				var filePath = './'+result.filename;
				console.log('storjId : ', result.storjfileid)
        // Fetching document from storage
        
        ipfs.files.get(result.storjfileid, function (err, files) {
          files.forEach((file) => {
            //console.log(file)
            var wstream = fs.createWriteStream(filePath);
            wstream.write(file.content);
            wstream.end();
            wstream.on('finish', function () {
              console.log('File has been downloaded');
              stat+=1;
              // Generating digest of the document
              hashFiles({files: filePath, algorithm: 'sha256'}, function(hashErr, hash) {
                // Clearing tempfile
                fs.stat(filePath, function (err, stats) {
                  //console.log(stats);//here we got all information of file in stats variable
                  if (err) console.error(err);
                  else
                    fs.unlink(filePath, function(err){
                      if(err) console.log(err);
                      console.log('file deleted successfully');
                    });
                });
                
                if(hashErr) console.log('file hash: error: ', hashErr);
                else{
                  console.log('Hash succeded', hash);
                  stat+=1;
                  
                  // Verifying signatures
                  console.log('IssuerId: ', result.issuerId);
                  Issuer.findById(result.issuerId, function(uDBerr, uDBres){
                    if (uDBerr) console.log(uDBerr)
                    else{
                      //console.log('Issuer DB Data: ', uDBres);
                      if(uDBres.pubkey == null || uDBres.pubkey == undefined){
                        req.flash('error', 'PUBLIC KEY is not found')
                        res.redirect('/univ/certificate/'+req.params.id)
                      }
                      else
                      verifySign(uDBres.pubkey, hash, encHash, uDBres.isRSA, function(isValid){
                        console.log(isValid)
                        if (isValid){
                          stat+=1			
                        }
                        // Checking revocation status
                        stat+=1;
                        console.log(stat);
                        console.log(result.receiverId);
                        Student.findById(result.receiverId, function(err, info){
                          if (err) console.log(err)
                          console.log('info: ', info)
                          res.render('univ/certificate', {data: result, title: 'Certificate', stat: stat, info: info, verify: true, layout: 'newlayout.hbs'})
                        })
                      })
                    }
                  })
                }
              });
            });
          })
        })
			}
    });
  });
});


/*
router.post('/certificate/:id', function (req, res) {
  var stat=0;
  // Fetching encrypted digest from blockchain
  Doc.findOne({p2bId: req.params.id}, function(err, result){
    request({
      url: 'http://159.65.153.243:8000/query',
      method: 'POST',
      json: {
        "channel"   : "mychannel",
        "chaincode" : "docchain",
        "function"  : "queryDocument",
        "args"      : result.p2bId
    }, function (reqErr, reqRes, reqBody){
			if (reqErr) console.log(reqErr);
			else{
				console.log('Encrypted hash obtained: ', reqBody);
        stat+=1;
        reqBody =  JSON.parse(reqBody.response)
				encHash=reqBody.signature;
				console.log('Encrypted hash: ', encHash);
				var filePath = './'+result.filename;
				console.log('storjId : ', result.storjfileid)
				// Fetching document from storage
				storj.resolveFile(bucketId, result.storjfileid, filePath, {
				progressCallback: function(progress, downloadedBytes, totalBytes) {
					console.log('Starting file download')
					},
				finishedCallback: function(stoErr) {
					if (stoErr) console.log(stoErr);
					else{
						console.log('File download complete');
						stat+=1;

						// Generating digest of the document
						hashFiles({files: filePath, algorithm: 'sha256'}, function(hashErr, hash) {
							// Clearing tempfile
							fs.stat(filePath, function (err, stats) {
								//console.log(stats);//here we got all information of file in stats variable
								if (err) console.error(err);
								else
									fs.unlink(filePath, function(err){
										if(err) console.log(err);
										console.log('file deleted successfully');
									});
							});
							
							if(hashErr) console.log('file hash: error: ', hashErr);
							else{
								console.log('Hash succeded', hash);
								stat+=1;
								
								// Verifying signatures
								console.log('IssuerId: ', result.issuerId);
								Issuer.findById(result.issuerId, function(uDBerr, uDBres){
									if (uDBerr) console.log(uDBerr)
									else{
                    //console.log('Issuer DB Data: ', uDBres);
                    if(uDBres.pubkey == null || uDBres.pubkey == undefined){
                      req.flash('error', 'PUBLIC KEY is not found')
                      res.redirect('/univ/certificate/'+req.params.id)
                    }
                    else
										verifySign(uDBres.pubkey, hash, encHash, uDBres.isRSA, function(isValid){
											console.log(isValid)
											if (isValid){
												stat+=1			
											}
                      // Checking revocation status
                      stat+=1;
                      console.log(stat);
                      Student.findById(result.receiverId, function(err, info){
												//console.log('info: ', info)
                        res.render('univ/certificate', {data: result, title: 'Certificate', stat: stat, info: info, verify: true})
                      })
										})
									}
								})
							}
						});
					}
				}});
			}
    });
  });
});
*/
router.get('/settings', authsys.ensureAuthenticated, function(req, res){
  Univ.findOne({dcid: req.user.id}, function(err, result){
    res.locals.user = req.user
    res.render('univ/set', {layout: 'univ.hbs', title: 'Settings', data: result, phead: 'Account Setting', icon: 'cogs'}); 
  })
})

router.post('/settings', authsys.ensureAuthenticated, function(req, res){
  console.log('Body parameters: ', req.body)
  var update = {}
  var msg;
  if(req.body.docType)
    {
      update.docType = req.body.docType
      msg = req.body.docType+' has been added to "Type of Document"'
    }
  if(req.body.signatory) 
  {
    update.signatory = req.body.signatory
    msg = req.body.signatory+' has been added to "Authorized Signatory"'  
  }
  if(req.body.docType && req.body.signatory)
    msg = '"'+req.body.signatory+'" & "'+req.body.docType+'" has been added to "Authorized Signatory" and "Type of Document" respectively'  
  req.flash('success', msg)
  Univ.findOneAndUpdate({dcid: req.user.id}, {$addToSet: update}, { "new": true, "upsert": true }, function(err, result){
    if(err) console.log(err)
    else{
      res.redirect('/univ/settings') 
    }
  })
})

router.post('/settings.signatory', authsys.ensureAuthenticated, function(req, res){
  console.log('Body parameters: ', req.body)
  var update = {}
  update.signDetail = req.body
  console.log('Update value', update)
  Univ.findOneAndUpdate({dcid: req.user.id}, {$pull: { 'signDetail': {  docType: req.body.docType }}}, function(err, result){
    if(err) console.log(err)
    else{
      //console.log("After Entry Removal", result)
      Univ.findOneAndUpdate({dcid: req.user.id}, {$addToSet: update}, { "new": true, "upsert": true }, function(err, result){
        if(err) console.log(err)
        else{
          //console.log('after signdetails update', result)
          req.flash('success', '"'+req.body.docType+'" is linked with "'+req.body.signatory+'"')
          res.redirect('/univ/settings')  
        }
      })
    }
  })
})

router.get('/settings.remove.signatory', authsys.ensureAuthenticated, function(req, res){
  console.log('Doctype-Signatory to be removed : ', req.query.docType)
  Univ.findOneAndUpdate({dcid: req.user.id}, {$pull: { 'signDetail': {  docType: req.query.docType }}}, function(err, result){
    if(err) console.log(err)
    else{
      req.flash('error', req.query.docType+" has been removed from the list of Authorised Signatories")
			res.redirect('/univ/settings')
    }
  })
})

module.exports = router;

/********** CUSTOM FUNCTIONS ***********/
var verifySign = function(pubkey, msg, signature, isRSA, callback){
  var isValid;
  if(isRSA){
    var sig = new jsrsaSign.crypto.Signature({"alg": "SHA256withRSA"});
    // initialize for signature validation
    sig.init(pubkey); // signer's certificate or pubkey
    // update data or msg
    sig.updateString(msg)
    // verify signature
    isValid = sig.verify(signature)
  }
  else{
    var sig = new jsrsaSign.crypto.Signature({"alg": 'SHA256withECDSA', "prov": "cryptojs/jsrsa"});
    sig.init({xy: pubkey, curve: 'secp256r1'});
    sig.updateString(msg);
    isValid = sig.verify(signature);
  }
  callback(isValid);
}

/******* Test routes and data *******/
/*
router.get('/confirm/', authsys.ensureAuthenticated, function(req, res){
  var docdata ={};
  docdata.udocId = 'testcert003'
  docdata.hashSign = '1234567899gr8r4g89g494g68g1g46g4seg4s'
  docdata.to = 'priyavenkat16295@gmail.com'
  docdata.from = req.user.email
  docdata.issuer = req.user.name
  //docdata.subject = 'Your "'+ docdata.udocId +'" from ' + docdata.issuer
  res.render('univ/confirm', {layout: 'univ.hbs', data: docdata})
})
*/
